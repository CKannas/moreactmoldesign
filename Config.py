"""
Package wide important path locations

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"

import os
import sys


if getattr(sys, 'frozen', False):
    # we are running in a |PyInstaller| bundle
    basedir = sys._MEIPASS
    BaseDir = os.path.join(basedir, "MOReactMolDesign")

    # RDKit paths
    RDKitBaseDir = os.path.join(basedir, "RDKit")
    RDKitDataDir = os.path.join(RDKitBaseDir, 'Data')
    # Set RDKit environment variable
    os.environ["RDBASE"] = RDKitBaseDir
else:
    # we are running in a normal Python environment
    basedir = os.path.dirname(__file__)
    BaseDir = basedir

    # RDKit paths
    from rdkit import RDConfig

    # Get RDKit paths
    try:
        # Non-Conda versions of RDKit
        RDKitBaseDir = RDConfig.RDBaseDir
        RDKitDataDir = RDConfig.RDDataDir
    except AttributeError:
        # Conda versions of RDKit
        RDKitBaseDir = RDConfig.RDCodeDir
        RDKitDataDir = RDConfig.RDDataDir
    # end try-except
# end if

# Find path of MOReactMolDesign
DataDir = os.path.join(BaseDir, "Data")
DocsDir = os.path.join(BaseDir, "Docs")
ReactEnumCodeDir = os.path.join(BaseDir, "ReactEnum")
ReactMOEACodeDir = os.path.join(BaseDir, "ReactMOEA")
ToolsDir = os.path.join(BaseDir, "CLI_Tools")
ThirdPartyDir = os.path.join(BaseDir, "Third_Party")
