Available Models(Scorers)
=========================
- Production Cost: Cumulative transformations cost.
- Structural Similarity: Fingerprint based similarity (Morgan Fingerprints & Tanimoto).
- Molecule Size (Molecular Weight MW)
- Number of Hydrogen Bond Donors (NHBD)
- Number of Hydrogen Bond Acceptors (NHBA)
- Topological Surface Area (TPSA)
- Number of Rotatable Bonds (NRotBonds)
- Partition Coefficient (cLogP)
- Molecular Refractivity (cMR)
- Water Solubility (cLogSw)


**Note:** *All of the above are based on RDKit's provided functionality.*