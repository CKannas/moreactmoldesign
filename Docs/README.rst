=====================================================
Multi-/Many-Objective Reaction based Molecular Design
=====================================================

This is a project for creating a collection of Multi-/Many-Objective Evolutionary Algorithms
for Reaction based design of novel molecules.

What is it?
===========

- Chemoinformatics tools for:

  - Reaction Enumeration
  - Reaction based Multi-Objective Molecular De Novo Design
  - Reaction based Many-Objective Molecular De Novo Design

- Multi-/Many-Objective Optimisation:

  - Genetic Programming Algorithm using DEAP package

- Development Information:

  - New BSD licensed
  - Python_ 3.5
  - Supports Windows/Linux

- Requirements:

  - DEAP_
  - RDKit_
  - NumPy_
  - SciPy_
  - Pandas_
  - Matplotlib_
  - Networkx_
  - PyDot_
  - Pyparsing_
  - ProgressBar_
  - Sphinx_

History
=======

  - September 2017: Development started by Christos Kannas.

License
=======

This document is copyright (C) 2017-2018 by Christos Kannas

This work is licensed under the Creative Commons Attribution 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

The intent of this license is similar to that of the RDKit itself.
In simple words: “Do whatever you want with it, but please give us some credit.”


.. Links
.. _Python: http://www.python.org
.. _DEAP: http://deap.readthedocs.org/en/master/
.. _RDKit: http://www.rdkit.org
.. _NumPy: http://www.numpy.org
.. _SciPy: http://www.scipy.org
.. _Pandas: http://pandas.pydata.org
.. _Matplotlib: http://matplotlib.org/
.. _Networkx: http://networkx.github.io/
.. _PyDot: https://github.com/carlos-jenkins/pydotplus
.. _Pyparsing: https://pypi.python.org/pypi/pyparsing
.. _ProgressBar: https://github.com/WoLpH/python-progressbar
.. _Sphinx: http://www.sphinx-doc.org/en/stable/