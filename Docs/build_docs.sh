#!/bin/bash

echo "Building API documentation..."
sphinx-apidoc -f -l -e -P -o ./API/ ../../MOReactMolDesign/

echo
echo "Cleaning HTML Documentation..."
make clean

echo
echo "Building HTML Documentation..."
make html

echo
echo "Done!!!"
