==========================================
How to build the developer's documentation
==========================================

The following instructions will guide you through the process of building
the developer's documentation for this software

Requirements
============

  - Python_ 3.5
  - Sphinx_

Instructions
============

1. Navigate to folder *Docs*.

2. Run command *./build_docs.sh*.

3. The HTML documentation will be build in *Docs/_build/html*. And can be browsed
   by opening *Docs/_build/html/index.html* in your preferred Internet Browser.

4. Congratulations you now have the HTML Developer's Documentation for
   N8BioHubChem package.


.. Links
.. _Python: http://www.python.org
.. _Sphinx: http://www.sphinx-doc.org/en/stable/