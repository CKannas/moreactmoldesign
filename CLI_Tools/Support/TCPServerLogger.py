from __future__ import print_function

"""
TCP Server Logger to be used by Parallel Single Step Enumeration.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"

import pickle
import logging
import logging.handlers
import struct

try:
    import SocketServer
except ImportError:
    import socketserver as SocketServer
# end try-except


class LogRecordStreamHandler(SocketServer.StreamRequestHandler):
    """Handler for a streaming logging request.

    This basically logs the record using whatever logging policy is
    configured locally.
    """

    def handle(self):
        """
        Handle multiple requests - each expected to be a 4-byte length,
        followed by the LogRecord in pickle format. Logs the record
        according to whatever policy is configured locally.
        """
        while True:
            chunk = self.connection.recv(4)
            if len(chunk) < 4:
                break
            # end if
            slen = struct.unpack('>L', chunk)[0]
            chunk = self.connection.recv(slen)
            while len(chunk) < slen:
                chunk = chunk + self.connection.recv(slen - len(chunk))
            # end while
            obj = self.unPickle(chunk)
            record = logging.makeLogRecord(obj)
            self.handleLogRecord(record)
        # end while

        return

    def unPickle(self, data):
        """"""
        return pickle.loads(data)

    def handleLogRecord(self, record):
        """"""
        # if a name is specified, we use the named logger rather than the one
        # implied by the record.
        if self.server.logname is not None:
            name = self.server.logname
        else:
            name = record.name
        # end if
        logger = logging.getLogger(name)
        # N.B. EVERY record gets logged. This is because Logger.handle
        # is normally called AFTER logger-level filtering. If you want
        # to do filtering, do it at the client end to save wasting
        # cycles and network bandwidth!
        logger.handle(record)

        return


# class LogRecordSocketReceiver(SocketServer.ThreadingTCPServer):
class LogRecordSocketReceiver(SocketServer.TCPServer):
    """
    Simple TCP socket-based logging receiver suitable for testing.
    """

    allow_reuse_address = 1

    def __init__(self, host='localhost',
                 # port=logging.handlers.DEFAULT_TCP_LOGGING_PORT,
                 port=0,  # Bind to a random socket in range 1024 to 65535
                 handler=LogRecordStreamHandler):
        """Initialise a TCP Socket Server to handle logging requests."""
        # SocketServer.ThreadingTCPServer.__init__(self, (host, port), handler)
        SocketServer.TCPServer.__init__(self, (host, port), handler)
        self.abort = 0
        self.timeout = 1
        self.logname = None

        return

    def getsockname(self):
        """Return IP and port used by socket."""
        return self.socket.getsockname()

    def serve_until_stopped(self):
        """"""
        import select
        abort = 0
        while not abort:
            rd, wr, ex = select.select([self.socket.fileno()],
                                       [], [],
                                       self.timeout)
            if rd:
                self.handle_request()
            else:
                abort = self.abort
            # end if
        # end while
        # Server Shutdown...
        self.shutdown()

        return
