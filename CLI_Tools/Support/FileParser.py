from __future__ import print_function

"""
Module that provides basic file manipulation functions

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"

import os
import gzip
import logging
import json

# RDKit
from rdkit import Chem
from rdkit.Chem.PropertyMol import PropertyMol

# N8BioHubChem
from MOReactMolDesign.ReactEnum import Reactions

logger = logging.getLogger('CLI_Tools.Support.FileParser')

# Properties list to be printed out
basePropsList = ["History"]

addPropsList = ["FormalCharge",
                "TPSA",
                "TPSARadius",
                "NumEthoxy",
                "NumCarbons",
                # "LengthLargeCarbonChain",
                # "MolVolume",
                "BranchFactor"]


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


def _historyTree(mol):
    """Create empty history tree.

    :param mol: Molecule object.
    :returns: History tree in json format."""
    # History tree
    histTree = dict()
    histTree["reaction"] = None
    histTree["reactants"] = list()
    histTree["molId"] = mol.GetProp("_Name")
    # JSON format
    histTree = json.dumps(histTree).replace(" ", "")

    return histTree


def SMILESFileReader(filePath, delimiter=' ',
                     smilesColumn=0, nameColumn=1, titleLine=True):
    """Read molecules from SMILES file

    :param filePath: File path of SMILES file
    :param delimiter: File delimiter
    :param smilesColumn: The index of the SMILES column
    :param nameColumn: The index of the Name/Id column
    :param titleLine: Set to True (1) if file has a title line,
                      else set to False (0)
    :returns: List of molecules
    """
    filePath = os.path.abspath(filePath)
    if not os.path.isfile(filePath):
        raise Error("'%s' is not a valid file." % (filePath))

    if filePath.endswith(".gz"):
        with gzip.open(filePath) as infile:
            inlines = infile.readlines()
            indata = ''.join(inlines)
        # end with
        suppl = Chem.SmilesMolSupplier()
        suppl.SetData(indata,
                      delimiter=delimiter,
                      titleLine=titleLine,
                      smilesColumn=smilesColumn,
                      nameColumn=nameColumn)
    else:
        suppl = Chem.SmilesMolSupplier(filePath,
                                       delimiter=delimiter,
                                       titleLine=titleLine,
                                       smilesColumn=smilesColumn,
                                       nameColumn=nameColumn)
    # end if
    molsList = [PropertyMol(x) for x in suppl if x is not None]
    # inform
    logger.info("Success: %d compounds parsed." % (len(molsList)))

    return molsList


def SMILESFileWriter(filePath, molsList, props=None):
    """Write molecules to SMILES file

    :param filePath: File path of SMILES file
    :param molsList: List of molecules
    :param props: List of properties names to write in the file
    :returns: File path of SMILES file
    """
    filePath = os.path.abspath(filePath)

    outfile = Chem.SmilesWriter(filePath, isomericSmiles=True)
    # Set the properties to be printed in the SMILES file
    if not props:
        molsList, props = _findAllProps(molsList)
    # end if
    outfile.SetProps(props)
    mol_counter = 0
    for mol in molsList:
        for prop in props:
            if not mol.HasProp(prop):
                if prop == "History":
                    # Create empty history tree
                    hist = _historyTree(mol)
                    mol.SetProp(prop, hist)
                else:
                    mol.SetProp(prop, "NaN")
                # end if
            else:
                value = mol.GetProp(prop)
                if value == " ":
                    mol.SetProp(prop, "NaN")
                # end if
            # end if
        # end for
        outfile.write(mol)
        outfile.flush()
        mol_counter += 1
    # end for
    outfile.close()

    # inform
    logger.info("Success: %d compounds saved to '%s'."
                % (mol_counter, filePath))

    return filePath


def SDFileWriter(filePath, molsList, props=None):
    """Write molecules to SD file

    :param filePath: File path of SD file
    :param molsList: List of molecules
    :param props: List of properties names to write in the file
    :returns: File path of SD file
    """
    filePath = os.path.abspath(filePath)
    outfile = Chem.SDWriter(filePath)
    # Set the properties to be printed in the SMILES file
    if not props:
        molsList, props = _findAllProps(molsList)
    # end if
    outfile.SetProps(props)
    mol_counter = 0
    for mol in molsList:
        for prop in props:
            if not mol.HasProp(prop):
                if prop == "History":
                    # Create empty history tree
                    hist = _historyTree(mol)
                    mol.SetProp(prop, hist)
                else:
                    mol.SetProp(prop, "NaN")
                # end if
            else:
                value = mol.GetProp(prop)
                if value == " ":
                    mol.SetProp(prop, "NaN")
                # end if
            # end if
        # end for
        outfile.write(mol)
        outfile.flush()
        mol_counter += 1
    # end for
    outfile.close()

    # inform
    logger.info("Success: %d compounds saved to '%s'."
                % (mol_counter, filePath))

    return filePath


def SMIRKSFileParser(filePath, delimiter=' ', titleLine=True,
                     reactIdCol=0, reactNameCol=1, reactSMIRKSCol=2):
    """Parse a SMIRKS file and populate the ReactionsContainer
    with the reactions.

    :param filePath: File path of SMILES file
    :param delimiter: File delimiter
    :param reactIdCol: The index of the Reaction Id column
    :param reactNameCol: The index of the Reaction Name column
    :param reactSMIRKSCol: The index of the Reaction SMIRKS column
    :param titleLine: Set to True (1) if file has a title line,
                      else set to False (0)
    :returns: List of Reactions, as a ReactionsContainer object.
    """
    parsed = 0
    success = 0
    titlelnLabels = list()
    reactionsList = Reactions.ReactionsContainer()
    #
    filePath = os.path.abspath(filePath)
    with open(filePath, "r") as rxnFile:
        lines = rxnFile.readlines()
        # starting line
        start = 0
        # if there is a title line then skip it
        if titleLine:
            start = 1
            titlelnLabels = lines[0].strip().split(delimiter)
        else:
            titlelnLabels = range(len(lines[0].strip().split(delimiter)))
        # end if
        for lnIdx, line in enumerate(lines[start:]):
            line = line.strip().split(delimiter)
            parsed += 1
            # Get reaction's id
            if reactIdCol in range(len(line)):
                reactId = line[reactIdCol]
            else:
                reactId = lnIdx
            # end if
            # Get reaction's name
            if reactNameCol in range(len(line)):
                reactName = line[reactNameCol]
            else:
                reactName = ""
            # end if
            try:
                # Create reaction object
                reaction = Reactions.ReactionContainer(reactId,
                                                       reactName,
                                                       line[reactSMIRKSCol])
                # Get any additional columns as additional properties
                for idx in range(len(line)):
                    if(idx == reactIdCol or
                       idx == reactNameCol or
                       idx == reactSMIRKSCol):
                        continue
                    # end if
                    propName = titlelnLabels[idx]
                    propVal = line[idx]
                    reaction.addProperty(propName, propVal)
                # end for
                reactionsList.addReaction(reaction)
                success += 1
            except ValueError:
                pass
        # end for
    # end with
    # inform
    logger.info("Success: %d/%d reactions parsed." % (success, parsed))

    return reactionsList

###############################################################################
#                                                                             #
#                                Utilities                                    #
#                                                                             #
###############################################################################


def _findAllProps(molsList):
    """Find all properties available in the molecules list.

    :param molsList: List of RDKit molecule objects.
    :returns: A list of molecule objects and a list with
              the names of their properties."""
    molProps = list()
    mols = list()
    for mol in molsList:
        mols.append(mol)
        props = list(mol.GetPropNames())
        for prop in props:
            if prop not in molProps:
                molProps.append(prop)
            # end if
        # end for
    # end for
    mols = iter(mols)

    return mols, molProps
