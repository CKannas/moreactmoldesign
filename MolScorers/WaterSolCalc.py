"""
Estimate water solubility of a molecule using chemical properties.

Based on J. S. Delaney, "ESOL: Estimating Aqueous Solubility
directly from Molecular Structure", Journal of Chemical Information and
Modelling, vol. 44, no. 3, pp. 1000-1005, May 2004.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""

__version__ = "0.1.0"

# Python imports
import logging

# MOReactMolDesign imports
from MOReactMolDesign.Optimizers import Scorer

# RDKit imports
from rdkit import Chem
from rdkit.Chem import Descriptors

logger = logging.getLogger('MolScorers.ESOL')


class ESOL(Scorer.scorerTemplate):
    """ESOL: Estimating Aqueous Solubility
       directly from Molecular Structure"""

    def __init__(self, debug=False):
        """"""
        self.name = "ESOL"
        self.obj_name = "clogSw"
        self.debug = debug

        return

    def _score(self, individual):
        """Estimate water solubility of a molecule using chemical properties.

        :param individual: An individual.
        :return: Individual's estimated water solubility (clogSw)."""

        # 1. Calculate MolWeight
        MolWeight = Descriptors.MolWt(individual)

        # 2. Calculate clogP
        clogP = Descriptors.MolLogP(individual)

        # 3. Calculate RotBonds
        RotBonds = Descriptors.NumRotatableBonds(individual)

        # 4. Calculate the number of aromatic heavyatoms in the molecule
        aromaticHeavyatoms = len(individual.GetSubstructMatches(
                                 Chem.MolFromSmarts("[a]")))
        # 5. Calculate total number of atoms in the molecule
        numAtoms = individual.GetNumAtoms()

        # 6. Calculate Aromatic Proportion
        AromProp = float(aromaticHeavyatoms) / numAtoms

        # 7. Calculate clogSw with coefficients calculated
        # using Ridge Regression with Cross-validation
        clogSw_value = 0.233743817233 \
                       - 0.74253027 * clogP \
                       - 0.00676305 * MolWeight \
                       + 0.01580559 * RotBonds \
                       - 0.35483585 * AromProp

        # 8. set property on individual
        individual.SetProp(self.obj_name, str(clogSw_value))

        # Inform
        smiles = Chem.MolToSmiles(individual, isomericSmiles=True)
        logger.info("Calculated water solubility (log mol/L) for %s : %f."
                    % (smiles, clogSw_value))

        return clogSw_value
