"""
Calculate Molecular Refractivity (cMR).

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""
__version__ = "0.1.0"

# Python imports
import logging

# MOReactMolDesign imports
from MOReactMolDesign.Optimizers import Scorer

# RDKit imports
from rdkit import Chem
from rdkit.Chem import Descriptors

logger = logging.getLogger('MolScorers.MolRefract')


class MolRefractCalc(Scorer.scorerTemplate):
    """Class for calculating Molecular Refractivity (cMR)."""
    def __init__(self, debug=False):
        """"""
        self.name = "MolRefractCalc"
        self.obj_name = "cMR"
        self.debug = debug

        return

    def _score(self, individual):
        """Calculate Molecular Refractivity (cMR).

        :param individual: An individual.
        :returns: The calculated Molecular Refractivity (cMR)."""

        # 1. Calculate Molecular Refractivity (cMR)
        cMR = Descriptors.MolMR(individual)

        # 2. Set property on individual
        individual.SetProp(self.obj_name, str(cMR))

        # Inform
        smiles = Chem.MolToSmiles(individual, isomericSmiles=True)
        logger.info("Calculated cMR for %s : %f."
                    % (smiles, cMR))

        return cMR
