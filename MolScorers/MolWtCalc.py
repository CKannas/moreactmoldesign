"""
Calculate molecular weight.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""
__version__ = "0.1.0"

# Python imports
import logging

# MOReactMolDesign imports
from MOReactMolDesign.Optimizers import Scorer

# RDKit imports
from rdkit import Chem
from rdkit.Chem import Descriptors

logger = logging.getLogger('MolScorers.MolWt')


class MolWtCalc(Scorer.scorerTemplate):
    """Class for calculating molecular weight."""
    def __init__(self, debug=False):
        """"""
        self.name = "MolWtCalc"
        self.obj_name = "MolWt"
        self.debug = debug

        return

    def _score(self, individual):
        """Calculate molecular weight.

        :param individual: An individual.
        :returns: The calculated molecular weight (MolWt)."""

        # 1. Calculate MolWeight
        MolWeight = Descriptors.MolWt(individual)

        # 2. Set property on individual
        individual.SetProp(self.obj_name, str(MolWeight))

        # Inform
        smiles = Chem.MolToSmiles(individual, isomericSmiles=True)
        logger.info("Calculated molecular weight for %s : %f."
                    % (smiles, MolWeight))

        return MolWeight
