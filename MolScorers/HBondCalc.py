"""
Calculate the number of Hydrogen Bond Donors(NHBD) and Acceptors(NHBA).

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""
__version__ = "0.1.0"

# Python imports
import logging

# MOReactMolDesign imports
from MOReactMolDesign.Optimizers import Scorer

# RDKit imports
from rdkit import Chem
from rdkit.Chem import Descriptors

logger = logging.getLogger('MolScorers.NHBond')


class HBondDonorsCalc(Scorer.scorerTemplate):
    """Class for calculating the number of Hydrogen Bond Donors."""
    def __init__(self, debug=False):
        """"""
        self.name = "HBondDonorsCalc"
        self.obj_name = "NHBD"
        self.debug = debug

        return

    def _score(self, individual):
        """Calculate the number of Hydrogen Bond Donors.

        :param individual: An individual.
        :returns: The calculated number of Hydrogen Bond Donors."""

        # 1. Calculate Hydrogen Bond Donors
        NHBDonors = Descriptors.NumHDonors(individual)

        # 2. Set properties on individual
        individual.SetProp(self.obj_name, str(NHBDonors))

        # Inform
        smiles = Chem.MolToSmiles(individual, isomericSmiles=True)
        logger.info("Calculated Number of HBDonors for %s : %f."
                    % (smiles, NHBDonors))

        return NHBDonors


class HBondAcceptorsCalc(Scorer.scorerTemplate):
    """Class for calculating the number of Hydrogen Bond Acceptors."""
    def __init__(self, debug=False):
        """"""
        self.name = "HBondAcceptorsCalc"
        self.obj_name = "NHBA"
        self.debug = debug

        return

    def _score(self, individual):
        """Calculate the number of Hydrogen Bond Acceptors.

        :param individual: An individual.
        :returns: The calculated number of Hydrogen Bond Acceptors."""

        # 1. Calculate Hydrogen Bond Acceptors
        NHBAcceptors = Descriptors.NumHAcceptors(individual)

        # 3. Set properties on individual
        individual.SetProp(self.obj_name, str(NHBAcceptors))

        # Inform
        smiles = Chem.MolToSmiles(individual, isomericSmiles=True)
        logger.info("Calculated Number of HBAcceptors for %s : %f."
                    % (smiles, NHBAcceptors))

        return NHBAcceptors
