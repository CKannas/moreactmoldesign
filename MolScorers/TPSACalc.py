"""
Calculate Topological Surface Area (TPSA).

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""
__version__ = "0.1.0"

# Python imports
import logging

# MOReactMolDesign imports
from MOReactMolDesign.Optimizers import Scorer

# RDKit imports
from rdkit import Chem
from rdkit.Chem import Descriptors

logger = logging.getLogger('MolScorers.TPSA')


class TPSACalc(Scorer.scorerTemplate):
    """Class for calculating Topological Surface Area (TPSA)."""
    def __init__(self, debug=False):
        """"""
        self.name = "TPSACalc"
        self.obj_name = "TPSA"
        self.debug = debug

        return

    def _score(self, individual):
        """Calculate Topological Surface Area (TPSA).

        :param individual: An individual.
        :returns: The calculated Topological Surface Area (TPSA)."""

        # 1. Calculate Topological Surface Area (TPSA)
        tpsa = Descriptors.TPSA(individual)

        # 2. Set property on individual
        individual.SetProp(self.obj_name, str(tpsa))

        # Inform
        smiles = Chem.MolToSmiles(individual, isomericSmiles=True)
        logger.info("Calculated TSPA for %s : %f."
                    % (smiles, tpsa))

        return tpsa
