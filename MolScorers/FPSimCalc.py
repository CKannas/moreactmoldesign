"""
Calculate Fingerprint based similarity (Morgan Fingerprints & Tanimoto).

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""
__version__ = "0.1.0"

# Python imports
import logging
import progressbar

# MOReactMolDesign imports
from MOReactMolDesign.Optimizers import Scorer

# RDKit imports
from rdkit import Chem
from rdkit.Chem import AllChem, \
                       DataStructs

logger = logging.getLogger('MolScorers.FPSim')


class FPSimCalc(Scorer.scorerTemplate):
    """Calculate Fingerprint based similarity (Morgan Fingerprints & Tanimoto)."""
    def __init__(self, target, debug=False):
        """

        :param target: A tuple/list with the target molecule and name.
        :param debug: Debug flag.
        """
        self.name = "FPSimCalc"
        self.debug = debug

        # Target Molecule and Name
        # Make sure param target is a tuple or list
        assert isinstance(target, tuple) or isinstance(target, list)
        # target must have 2 entries
        assert len(target) == 2
        # The 1st is the SMILES
        self.target_mol = Chem.MolFromSmiles(target[0])
        # The 2nd is the name of the target molecule
        self.target_mol.SetProp("_Name", str(target[1]))
        # Setup property name
        self.obj_name = "FPSim_" + Chem.MolToSmiles(self.target_mol, isomericSmiles=True)

        return

    def calcFitness(self, population):
        """Calculate fitness scores for the entire population,

        :param population: List of individuals.
        :returns: List of calculated fitness scores."""
        # Generate Morgan Fingerprints
        target_fp = AllChem.GetMorganFingerprint(self.target_mol, 2)
        pop_fps = [AllChem.GetMorganFingerprint(indi, 2) for indi in population]

        # Progress Bar
        widgets = [self.name, " ",
                   progressbar.Percentage(), " ",
                   progressbar.ETA(), " ",
                   progressbar.Bar()]
        pbar = progressbar.ProgressBar(widgets=widgets, max_val=len(population))
        # Progress bar start
        pbar.start()
        # Bulk Tanimoto Similarity
        fp_sim_values = DataStructs.BulkTanimotoSimilarity(target_fp, pop_fps)

        count = 0
        #
        fitness_scores = list()
        for idx, individual in enumerate(population):
            score_value = fp_sim_values[idx]
            individual.SetProp(self.obj_name, str(score_value))
            fitness_scores.append(score_value)
            count += 1
            pbar.update(count)
        # end for
        # Progress bar finish
        pbar.finish()

        # assert
        assert(len(fitness_scores) == len(population))

        return fitness_scores
