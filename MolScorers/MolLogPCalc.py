"""
Calculate Partition Coefficient(LogP).

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""
__version__ = "0.1.0"

# Python imports
import logging

# MOReactMolDesign imports
from MOReactMolDesign.Optimizers import Scorer

# RDKit imports
from rdkit import Chem
from rdkit.Chem import Descriptors

logger = logging.getLogger('MolScorers.MolLogP')


class MolLogPCalc(Scorer.scorerTemplate):
    """Class for calculating Partition Coefficient(LogP)."""
    def __init__(self, debug=False):
        """"""
        self.name = "MolLogPCalc"
        self.obj_name = "clogP"
        self.debug = debug

        return

    def _score(self, individual):
        """Calculate Partition Coefficient(LogP).

        :param individual: An individual.
        :returns: The calculated Partition Coefficient(clogP)."""

        # 1. Calculate clogP
        clogP = Descriptors.MolLogP(individual)

        # 2. Set property on individual
        individual.SetProp(self.obj_name, str(clogP))

        # Inform
        smiles = Chem.MolToSmiles(individual, isomericSmiles=True)
        logger.info("Calculated LogP for %s : %f."
                    % (smiles, clogP))

        return clogP
