from __future__ import print_function

"""
Unit testing of MolScorers module.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"

# Python Imports
import os
import unittest

# MOReactMolDesign Imports
from MOReactMolDesign import Config
from MOReactMolDesign.CLI_Tools.Support import FileParser


# TODO: To be updated for checking scorers initialisation from json string.

class TestCase(unittest.TestCase):
    """Test Case for MolScores module."""

    def setUp(self):
        """Setup environment."""
        # We only need a list of molecules
        # Parse SMILES file
        self.smilesFile = os.path.join(Config.DataDir,
                                       "N8BioHubFeedStocksUsefulReactants.smi")
        self.molsList = FileParser.SMILESFileReader(self.smilesFile)

        return

    def tearDown(self):
        """Cleanup environment."""

        return

    def test_MolWtCalc(self):
        """Testing MolWtCal module."""
        # Import
        from MOReactMolDesign.MolScorers import MolWtCalc

        # Initialise scorer
        MolWtScorer = MolWtCalc.MolWtCalc()

        # Calculate scores
        molwt_scores = MolWtScorer.calcFitness(self.molsList)

        # Check if there are of the same length
        assert len(molwt_scores) == len(self.molsList)
        # Check if the values are the same
        for idx, mol in enumerate(self.molsList):
            # GetProp returns string...
            assert molwt_scores[idx] == float(mol.GetProp(MolWtScorer.obj_name))
        # end for

        return

    def test_HBondDonorCalc(self):
        """Testing HBondDonorCalc module."""
        # Import
        from MOReactMolDesign.MolScorers import HBondCalc

        # Initialise scorer
        HBondScorer = HBondCalc.HBondDonorsCalc()

        # Calculate scores
        hbond_scores = HBondScorer.calcFitness(self.molsList)

        # Check if there are of the same length
        assert len(hbond_scores) == len(self.molsList)
        # Check if the values are the same
        for idx, mol in enumerate(self.molsList):
            # GetProp returns string...
            assert hbond_scores[idx] == float(mol.GetProp(HBondScorer.obj_name))
        # end for

        return

    def test_HBondAcceptorCalc(self):
        """Testing HBondAcceptoralc module."""
        # Import
        from MOReactMolDesign.MolScorers import HBondCalc

        # Initialise scorer
        HBondScorer = HBondCalc.HBondAcceptorsCalc()

        # Calculate scores
        hbond_scores = HBondScorer.calcFitness(self.molsList)

        # Check if there are of the same length
        assert len(hbond_scores) == len(self.molsList)
        # Check if the values are the same
        for idx, mol in enumerate(self.molsList):
            # GetProp returns string...
            assert hbond_scores[idx] == float(mol.GetProp(HBondScorer.obj_name))
        # end for

        return

    def test_MolLogPCalc(self):
        """Testing MolLogPCalc module."""
        # Import
        from MOReactMolDesign.MolScorers import MolLogPCalc

        # Initialise scorer
        MolLogPScorer = MolLogPCalc.MolLogPCalc()

        # Calculate scores
        clogp_scores = MolLogPScorer.calcFitness(self.molsList)

        # Check if there are of the same length
        assert len(clogp_scores) == len(self.molsList)
        # Check if the values are the same
        for idx, mol in enumerate(self.molsList):
            # GetProp returns string...
            assert clogp_scores[idx] == float(mol.GetProp(MolLogPScorer.obj_name))
        # end for

        return

    def test_MRCalc(self):
        """Testing MRCalc module."""
        # Import
        from MOReactMolDesign.MolScorers import MRCalc

        # Initialise scorer
        MRScorer = MRCalc.MolRefractCalc()

        # Calculate scores
        mr_scores = MRScorer.calcFitness(self.molsList)

        # Check if there are of the same length
        assert len(mr_scores) == len(self.molsList)
        # Check if the values are the same
        for idx, mol in enumerate(self.molsList):
            # GetProp returns string...
            assert mr_scores[idx] == float(mol.GetProp(MRScorer.obj_name))
        # end for

        return

    def test_RotBondsCalc(self):
        """Testing RotBondsCalc module."""
        # Import
        from MOReactMolDesign.MolScorers import RotBondsCalc

        # Initialise scorer
        RotBondScorer = RotBondsCalc.RotBondCalc()

        # Calculate scores
        rb_scores = RotBondScorer.calcFitness(self.molsList)

        # Check if there are of the same length
        assert len(rb_scores) == len(self.molsList)
        # Check if the values are the same
        for idx, mol in enumerate(self.molsList):
            # GetProp returns string...
            assert rb_scores[idx] == float(mol.GetProp(RotBondScorer.obj_name))
        # end for

        return

    def test_TPSACalc(self):
        """Testing MRCalc module."""
        # Import
        from MOReactMolDesign.MolScorers import TPSACalc

        # Initialise scorer
        TPSAScorer = TPSACalc.TPSACalc()

        # Calculate scores
        tpsa_scores = TPSAScorer.calcFitness(self.molsList)

        # Check if there are of the same length
        assert len(tpsa_scores) == len(self.molsList)
        # Check if the values are the same
        for idx, mol in enumerate(self.molsList):
            # GetProp returns string...
            assert tpsa_scores[idx] == float(mol.GetProp(TPSAScorer.obj_name))
        # end for

        return

    def test_WaterSolCalc(self):
        """Testing WaterSolCalc module."""
        # Import
        from MOReactMolDesign.MolScorers import WaterSolCalc

        # Initialise scorer
        WaterSolScorer = WaterSolCalc.ESOL()

        # Calculate scores
        clogSw_scores = WaterSolScorer.calcFitness(self.molsList)

        # Check if there are of the same length
        assert len(clogSw_scores) == len(self.molsList)
        # Check if the values are the same
        for idx, mol in enumerate(self.molsList):
            # GetProp returns string...
            assert clogSw_scores[idx] == float(mol.GetProp(WaterSolScorer.obj_name))
        # end for

        return

    def test_FPSimCalc(self):
        """Testing FPSimCalc module."""
        # Import
        from MOReactMolDesign.MolScorers import FPSimCalc
        from rdkit import Chem

        # Initialise target molecule
        target = ("CCCCCCCCCCCCCCCCO", "26")

        # Initialise scorer
        FPSimScorer = FPSimCalc.FPSimCalc(target)

        # Calculate scores
        fpsim_scores = FPSimScorer.calcFitness(self.molsList)

        # Check if there are of the same length
        assert len(fpsim_scores) == len(self.molsList)
        # Check if the values are the same
        for idx, mol in enumerate(self.molsList):
            # GetProp returns string...
            assert fpsim_scores[idx] == float(mol.GetProp(FPSimScorer.obj_name))
        # end for

        return

    def test_MolCostCalc(self):
        """Testing MolCostCalc module."""
        # Import
        from MOReactMolDesign.MolScorers import MolCost

        # Initialise scorer
        MolCostScorer = MolCost.MolCost(reactionsCost={})

        # Calculate scores
        molcost_scores = MolCostScorer.calcFitness(self.molsList)

        # Check if there are of the same length
        assert len(molcost_scores) == len(self.molsList)
        # Check if the values are the same
        for idx, mol in enumerate(self.molsList):
            # GetProp returns string...
            assert molcost_scores[idx][0] == float(mol.GetProp(MolCostScorer.obj_name))
            assert molcost_scores[idx][1] == float(mol.GetProp("NumTransforms"))
        # end for

        return


if __name__ == "__main__":
    unittest.main()
