"""
Calculate the Number of Rotatable Bonds (NRotBonds).

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""
__version__ = "0.1.0"

# Python imports
import logging

# MOReactMolDesign imports
from MOReactMolDesign.Optimizers import Scorer

# RDKit imports
from rdkit import Chem
from rdkit.Chem import Descriptors

logger = logging.getLogger('MolScorers.NRotBond')


class RotBondCalc(Scorer.scorerTemplate):
    """Class for calculating the Number of Rotatable Bonds (NRotBonds)."""
    def __init__(self, debug=False):
        """"""
        self.name = "RotBondCalc"
        self.obj_name = "NRotBonds"
        self.debug = debug

        return

    def _score(self, individual):
        """Calculate the Number of Rotatable Bonds (NRotBonds).

        :param individual: An individual.
        :returns: The calculated Number of Rotatable Bonds (NRotBonds)."""

        # 1. Calculate Hydrogen Bond Donors
        NRotBonds = Descriptors.NumRotatableBonds(individual)

        # 2. Set properties on individual
        individual.SetProp(self.obj_name, str(NRotBonds))

        # Inform
        smiles = Chem.MolToSmiles(individual, isomericSmiles=True)
        logger.info("Calculated Number of Rotatable Bonds for %s : %f."
                    % (smiles, NRotBonds))

        return NRotBonds
