"""
Cost of producting the molecule based on accumulated transformation(s) cost.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""

__version__ = "0.1.0"

# Python imports
import logging
import json

# MOReactMolDesign imports
from MOReactMolDesign.Optimizers import Scorer
from MOReactMolDesign.Utilities import JSONUtils

logger = logging.getLogger('MolScorers.MolCost')


class MolCost(Scorer.scorerTemplate):
    """Calculate the cost of producing a molecule"""

    def __init__(self, reactionsCost, debug=False):
        """Initialise MolCost.

        :param reactionsCost: Dictionary of reactions id (key) and its assorted cost (value).
        :param debug: Enable/disable debug mode."""
        self.name = "MolCost"
        self.obj_name = "Cost"
        # Dictionary of reactions id (key) and its assorted cost (value)
        self._reactionsCost = reactionsCost
        self.debug = debug

        return

    def _score(self, individual):
        """Calculate compound's creation cost and history height.

        :param individual: The compound.
        :return: Compound's creation cost and number of transformations used."""
        accum_cost = 0
        hist_height = 0

        # Check if individual has production history...
        if individual.HasProp("History"):
            # Get individual's history, tree like structure in JSON format
            indiHistory = individual.GetProp("History")
            # Parse JSON to dictionary
            indiHistory = json.loads(indiHistory, object_hook=JSONUtils.convert)
            # Calculate history tree height
            hist_height = self._treeHeight(indiHistory)
            # Calculate accumulated cost
            accum_cost = self._accumCost(indiHistory)
        # end if

        # Score values
        score_value = (accum_cost, hist_height)
        # Save score values to compound's data structure
        individual.SetProp(self.obj_name, str(accum_cost))
        individual.SetProp("NumTransforms", str(hist_height))

        return score_value

    def _treeHeight(self, tree):
        """Measure the height of compound's history tree

        :param tree: Compound's history as a tree like structure.
        :return: The height of the tree."""
        height = 0
        for reactant in tree["reactants"]:
            # debug
            logger.debug("%s" % (reactant))
            height += self._treeHeight(reactant)
        # end for
        if tree["reaction"]:
            height += 1
        # end if

        # debug
        logger.debug("%s" % (height))

        return height

    def _accumCost(self, history):
        """Calculate compound's creation cost.

        :param history: Compound's creation history.
        :return: Compound's accumulated cost"""
        cost = 0
        for reactant in history["reactants"]:
            # debug
            logger.debug("%s" % (reactant))
            cost += self._accumCost(reactant)
        # end for
        if history["reaction"]:
            rid = history["reaction"]
            # Convert int to string...
            if isinstance(rid, int):
                rid = str(rid)
            # end if
            cost += self._reactionsCost[rid]
        # end if

        # debug
        logger.debug("%s" % (cost))

        return cost
