"""
A module for commonly used desirability functions.
The desirability functions are Derringer and Suich.

Derringer, G., and Suich, R., (1980),
"Simultaneous Optimization of Several Response Variables,"
Journal of Quality Technology,
12, 4, 214-219.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""

__version__ = "0.1.0"

import math
import numpy as np


LARGE_NUM = 100


###############################################################################
#                                                                             #
#               Logistic curve based desirability functions                   #
#                                                                             #
###############################################################################


def logCurve(x, h=0, s=1.0):
    """Logistic curve.

    :param x: Value on x-axis.
    :param h: Horizontal position of the curve.
              Positive values shift the curve to the left.
    :param s: Slope of the curve. Increasing it make the curve steeper,
              and its sign determins whether the curve slopes
              upwards or downwards.
    :returns: Logistic Curve value."""

    exp = np.exp((h + s * x))
    result = exp / (1 + exp)

    return result


def minimizeLC(x, Low=float(-LARGE_NUM), Upper=float(LARGE_NUM)):
    """Minimize Logistic Curve.
    Parameters *Low* and *Upper* are used to set parameted *h*
    of *logCurve* function. Parameter *s* of *logCurve* is set to -1.0.

    :param x: Value on x-axis.
    :param Low: Lower value for *x* range.
    :param Upper: Upper value for *x* range.
    :returns: Minimized Logistic Curve value."""

    return logCurve(x, h=np.mean([Low, Upper]), s=-1.0)


###############################################################################
#                                                                             #
#          The desirability functions from Derringer and Suich                #
#                                                                             #
###############################################################################


def minimizeD(x, Low=float(-LARGE_NUM), Upper=float(LARGE_NUM), power=1):
    """Minimize desirability function.

    :param x: Value on x-axis.
    :param Low: Lower value for *x* range.
    :param Upper: Upper value for *x* range.
    :returns: Scaled value in range of 0 to 1
              (returns 1 when *x* lower than *Low*)."""
    Low = float(Low)
    Upper = float(Upper)
    if x < Low:
        return 1.0
    elif x >= Low and x <= Upper:
        tmp = (x - Upper) / (Low - Upper)
        return (math.pow(tmp, power))
    else:  # x > Upper
        return 0
    # end if


def maximizeD(x, Low=float(-LARGE_NUM), Upper=float(LARGE_NUM), power=1):
    """Maximize desirability function.

    :param x: Value on x-axis.
    :param Low: Lower value for *x* range.
    :param Upper: Upper value for *x* range.
    :returns: Scaled value in range of 0 to 1
              (returns 1 when *x* greater than *Upper*)."""
    Low = float(Low)
    Upper = float(Upper)
    if x < Low:
        return 0
    elif x >= Low and x <= Upper:
        tmp = (x - Low) / (Upper - Low)
        return (math.pow(tmp, power))
    else:  # x > Upper
        return 1
    # end if


def rangeD(x, Low, Upper, powerLow=1, powerUp=1):
    """Ranged desirability function.

    :param x: Value on x-axis.
    :param Low: Lower value for *x* range.
    :param Upper: Upper value for *x* range.
    :returns: Scaled value in range of 0 to 1
              (returns 1 when *x* between *Low* and *Upper*)."""
    Low = float(Low)
    Upper = float(Upper)
    if x < Low:
        return maximizeD(x, Upper=Low)
    elif x >= Low and x <= Upper:
        return 1
    else:  # x > Upper
        return minimizeD(x, Low=Upper)
    # end if
