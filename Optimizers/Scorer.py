"""
Scorer template for Evolutionary Algorithms

A module for single objective and multiple objective scorer template.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""

__version__ = "0.1.0"

import progressbar


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class scorerTemplate(object):
    """Template scorer class"""

    def __init__(self):
        """"""
        self.name = "scorerTemplate"

        return

    def calcFitness(self, population):
        """Calculate fitness scores for the entire population,

        :param population: List of individuals.
        :returns: List of calculated fitness scores."""
        # Progress Bar
        widgets = [self.name, " ",
                   progressbar.Percentage(), " ",
                   progressbar.ETA(), " ",
                   progressbar.Bar()]
        pbar = progressbar.ProgressBar(widgets=widgets, max_val=len(population))
        # Progress bar start
        pbar.start()
        count = 0
        #
        fitness_scores = list()
        for individual in population:
            score_value = self._score(individual)
            fitness_scores.append(score_value)
            count += 1
            pbar.update(count)
        # end for
        # Progress bar finish
        pbar.finish()

        # assert
        assert(len(fitness_scores) == len(population))

        return fitness_scores

    def _score(self, individual):
        """Dummy _score method.

        :param individual: An individual.
        :returns: A score value."""
        score_value = 0

        return score_value


class MOScorer(scorerTemplate):
    """Multiobjective scorer class"""

    def __init__(self, objectives, debug=0):
        """"""
        self.name = "MOScorer"
        self.objectives = objectives
        self.num_objectives = len(objectives)
        #
        self.debug = debug

        return

    def calcFitness(self, population):
        """Calculate multiple fitness scores for the entire population,

        :param population: List of individuals.
        :returns: List of calculated fitness scores."""
        fitness_scores = list()
        #
        for objScorerName, objScorer in self.objectives.items():
            scores = objScorer.calcFitness(population)
            #
            assert(len(scores) == len(population))
        # end for
        # Progress Bar
        widgets = [self.name, " ",
                   progressbar.Percentage(), " ",
                   progressbar.ETA(), " ",
                   progressbar.Bar()]
        pbar = progressbar.ProgressBar(widgets=widgets, maxval=len(population))
        # Progress bar start
        pbar.start()
        count = 0
        for individual in population:
            mo_score = individual.getMOScore(self.objectives.keys())
            fitness_scores.append(mo_score)
            count += 1
            pbar.update(count)
        # end for
        # Progress bar finish
        pbar.finish()

        return fitness_scores
