from __future__ import print_function

"""
Unit testing of ReactMOGP module.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"

# Python Imports
import os
import unittest
import json

# MOReactMolDesign Imports
from MOReactMolDesign import Config
from MOReactMolDesign.CLI_Tools.Support import FileParser
from MOReactMolDesign.Utilities import PickleUtils, \
                                       JSONUtils
from MOReactMolDesign.ReactMOEA import ReactMOGP


class TestCase(unittest.TestCase):
    """Test case class for ReactMOGP."""

    def setUp(self):
        """Setup environment."""
        # Parse Molecules (SMILES file)
        smilesFile = os.path.join(Config.DataDir,
                                  "N8BioHubFeedStocksUsefulReactants.smi")
        self.molsList = FileParser.SMILESFileReader(smilesFile)
        # Parse Transformations (SMIRKS file)
        reactionsFile = os.path.join(Config.DataDir,
                                     "N8BioHubReactions.smirks")
        self.reactionsList = FileParser.SMIRKSFileParser(reactionsFile)

        print("\nParsing input parameters from JSON file...")
        # 1. Parse JSON file
        paramsJSONFile = os.path.join(Config.DataDir,
                                      "reactmogp_params.json")
        with open(paramsJSONFile) as fp:
            input_params = json.load(fp, object_hook=JSONUtils.convert)
        # end with
        #print(input_params)

        # 2. Add starting molecules
        input_params["molecules"] = self.molsList
        # 3. Add transformations
        input_params["reactions"] = self.reactionsList

        print("Initializing MOReactGP...")
        self.MOReactGP = ReactMOGP.MOReactGenProg(input_params, debug=True)
        print("MOReactGP has been initialized...")

        # CleanUp Functions (LIFO)
        # 5, Test CleanUp...
        self.addCleanup(self._cleanup)
        # 6. CleanUp...
        self.addCleanup(self.MOReactGP._cleanUp)

        return

    def tearDown(self):
        """Tear down environment."""
        self.molsList = None
        self.reactionsList = None

        return

    def test_initIntStore(self):
        """Test initialization of MOReactGenProg class."""
        # Check initialization
        print("Checking MOReactGP's internal storage...")
        # 1. Check Starting Molecules
        assert self.MOReactGP.starting_molecules is self.molsList, "Starting Molecules list is not the same."
        # 2. Check transformations
        assert self.MOReactGP.reactions is self.reactionsList, "Transformations Container is not the same."
        # 3 Check internal storage
        assert isinstance(self.MOReactGP.internal_storage, dict), "Internal storage is not a dictionary."
        # 4. Check GP Search Parameters
        # 4.1 Population size
        assert self.MOReactGP.internal_storage["population_size"] == 100, \
            "Wrong initialization of population size (%s)." % self.MOReactGP.internal_storage["population_size"]
        # 4.2 Maximum Iterations
        assert self.MOReactGP.internal_storage["max_iter"] == 100, \
            "Wrong initialization of maximum iterations number (%s)." % self.MOReactGP.internal_storage["max_iter"]
        # 4.3 Diversity Mechanism
        assert self.MOReactGP.internal_storage["diversity_type"] == "chemical", \
            "Wrong initialization of diversity mechanism."
        # 4.4 Many Objective Preferability Operator
        assert isinstance(self.MOReactGP.internal_storage["preferability_operator"], dict), \
            "Wrong initialization of preferability operator."
        # 4.5 Models Testing
        assert isinstance(self.MOReactGP.internal_storage["models"], list), \
            "Wrong initialization of models list."
        assert isinstance(self.MOReactGP.internal_storage["quantities"], list), \
            "Wrong initialization of quantities list."
        assert isinstance(self.MOReactGP.internal_storage["conditions"], list), \
            "Wrong initialization of conditions list."
        assert isinstance(self.MOReactGP.internal_storage["objectives"], list), \
            "Wrong initialization of objectives list."
        assert isinstance(self.MOReactGP.internal_storage["scorers"], list), \
            "Wrong initialization of scorers list."

        # 4.6 Archives Testing
        # 4.6.1 Elites Archive
        assert os.path.exists(self.MOReactGP.internal_storage["elite_archive"]), \
            "Elite Archive temporary file DOES NOT EXIST!"
        # 4.6.2 Archive
        assert os.path.exists(self.MOReactGP.internal_storage["archive"]), \
            "Archive temporary file DOES NOT EXIST!"
        # 4.6.3 Solutions Archive
        assert os.path.exists(self.MOReactGP.internal_storage["solutions_archive"]), \
            "Solutions Archive temporary file DOES NOT EXIST!"

        # 4.7 Check Pickled Structures
        # 4.7.1 Pareto Approximation pickled structure
        assert isinstance(self.MOReactGP.internal_storage["pareto_approximation"], bytes), \
            "Pickled Pareto Approximation structure is not a string."
        # 4.7.2 Pareto Approximation unpickled structure
        assert isinstance(PickleUtils.unpickleStrToObj(self.MOReactGP.internal_storage["pareto_approximation"]),
                          dict), "Unpickled Pareto Approximation structure is not a dictionary."
        # 4.7.3 Solutions pickled structure
        assert isinstance(self.MOReactGP.internal_storage["solutions"], bytes), \
            "Pickled Solutions structure is not a string."
        # 4.7.4 Solutions unpickled structure
        assert isinstance(PickleUtils.unpickleStrToObj(self.MOReactGP.internal_storage["solutions"]),
                          list), "Unpickled Solutions structure is not a list."

        print("Checking completed...")

        return

    def test_initScorers(self):
        """Test scorers initialization."""

        print("Initializing Scorers...")
        self.MOReactGP.initScorers()

        print("Check Scorers...")
        assert len(self.MOReactGP.internal_storage["scorers"]) == self.MOReactGP.internal_storage["num_objectives"], \
            "Number of scorers (%d) is not the same as the number of objectives (%d)." \
            % (len(self.MOReactGP.internal_storage["scorers"]), self.MOReactGP.internal_storage["num_objectives"])

        for scorer in self.MOReactGP.internal_storage["scorers"]:
            assert "handler" in scorer, "Scorer %s has not been initialized correctly." % scorer["model_name"]
        # end for
        print("Checking completed...")

        return

    def _cleanup(self):
        """Test cleanup operation."""
        # 4.6.1 Elites Archive
        assert not os.path.exists(self.MOReactGP.internal_storage["elite_archive"]), \
            "Elite Archive temporary file EXISTS!"
        # 4.6.2 Archive
        assert not os.path.exists(self.MOReactGP.internal_storage["archive"]), \
            "Archive temporary file EXISTS!"
        # 4.6.3 Solutions Archive
        assert not os.path.exists(self.MOReactGP.internal_storage["solutions_archive"]), \
            "Solutions Archive temporary file EXISTS!"

        return


if __name__ == '__main__':
    unittest.main()
