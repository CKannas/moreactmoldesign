from __future__ import print_function

"""
Multi-\Many- Objective Genetic Programming Algorithm for Molecular Design

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""

__version__ = "0.1.0"

# Python Imports
import os
import logging
import pprint
from datetime import datetime
import tempfile
import random

import copy

# Ploting Imports
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import networkx as nx

# MOReactMolDesign Imports
from MOReactMolDesign import Config
from MOReactMolDesign.Utilities import PickleUtils
from MOReactMolDesign.CLI_Tools.Support import FileParser
from MOReactMolDesign.ReactMOEA import DEAPGenProgMod

# RDKit Imports
from rdkit import Chem
from rdkit.Chem import PandasTools
from rdkit.Chem import Draw

random.seed(69)

# Global Variables
errors = list()
usefulReagentsFile = os.path.join(Config.DataDir, 'UsefulReagents_v1b.smi')
usefulReagents = FileParser.SMILESFileReader(usefulReagentsFile)

# Logger
logger = logging.getLogger('ReactMOEA.ReactMOGP')


###############################################################################
#               Customised Genetic Programming Algorithm                      #
###############################################################################
class MOReactGenProg(object):
    """
    Custom class for creating a Multi-/Many-Objective Reaction based
    Genetic Programming Algorithm for Molecular Design.

    It uses functionality from DEAP package in addition to some custom methods.
    """

    def __init__(self, parameters, debug=False):
        """
        Initialize an instance of MOReactGenProg class.

        :param parameters: A dictionary with the algorithm parameters.
        :param debug: Handler for enable/disable debug mode.
        """
        # Things that have to be initialised...
        # Will be expecting 1 & 2 from the calling module.
        # Will need to initialize 3 based on the input from the calling module.
        # Will use 4 to initialize and run a GP.

        self.debug = debug

        # 1. List of Molecules (Starting Ingredients) -> list
        self.starting_molecules = parameters.get("molecules", None)
        # 2. List of Reactions -> ReactionsContainer
        self.reactions = parameters.get("reactions", None)
        # 3. List of Scorers -> 1+ Scorer Objects (MolScorers module)
        # 4. GP parameters & settings...

        # Initialize internal storage structure
        self.internal_storage = dict()
        # Initialize internal storage
        self._initIntStore(parameters)

        return

    def _initIntStore(self, params):
        """Initialize internal storage

        :param params: Input parameters.
        :returns: Internal storage as a dictionary structure."""
        # initialize internal storage structure

        # Setup logs directory
        self.internal_storage["logs_dir"] = ('MOReactMolDesign_log_progress_%s'
                                             % (datetime.now().strftime("%Y%m%d_%H%M%S")))
        if not os.path.exists(self.internal_storage["logs_dir"]):
            os.makedirs(self.internal_storage["logs_dir"])
        # end if
        # change directory...
        os.chdir(self.internal_storage["logs_dir"])

        # population size
        self.internal_storage["population_size"] = params.get("numberofresultsreturned", 50)
        # max iterations
        self.internal_storage["max_iter"] = params.get("numberofiterations", 50)
        # current iteration
        self.internal_storage["curr_iter"] = 1
        # diversity option ("chemical or "property")
        self.internal_storage["diversity_type"] = params.get("diversitytype", "chemical")
        # preferability operator used for many-objective optimisation
        self.internal_storage["preferability_operator"] = params.get("preferabilityoperator", False)

        ###########################################################################
        #                          Updated JSON input                             #
        ###########################################################################
        # The following information must be combined to create instances of       #
        # scorers which should be called by multiScoring function in parallel.    #
        ###########################################################################
        #
        # TODO: Think about the structure of the information regarding available scorers
        # TODO: Update section below accordingly
        # TODO: Update related modules/methods accordingly
        # Note: Should this be similar to the input we had in N8BioHub?
        # Note: Should we do something simpler?
        #
        # models information
        self.internal_storage["models"] = params.get("models", list())
        # conditions and criteria information
        self.internal_storage["quantities"] = params.get("quantities", list())
        # conditions values
        self.internal_storage["conditions"] = params.get("conditions", list())
        # objectives (updated)
        self.internal_storage["objectives"] = params.get("quantityCriteria", list())
        # scorers information
        self.internal_storage["scorers"] = list()
        ###########################################################################
        # number of objectives
        self.internal_storage["num_objectives"] = len(self.internal_storage["objectives"])

        # Initialize Archives Files & Objects
        # Elites Archive (Pickled File)
        fd, tname = tempfile.mkstemp()
        with os.fdopen(fd, "wb") as f:
            PickleUtils.pickleObjToFile(dict(), f)
        # end with
        self.internal_storage["elite_archive"] = tname
        # Archive (Pickled File)
        fd, tname = tempfile.mkstemp()
        with os.fdopen(fd, "wb") as f:
            PickleUtils.pickleObjToFile({0: {}}, f)
        # end with
        self.internal_storage["archive"] = tname
        # Solutions Archive (Pickled File)
        fd, tname = tempfile.mkstemp()
        with os.fdopen(fd, "wb") as f:
            PickleUtils.pickleObjToFile(dict(), f)
        # end with
        self.internal_storage["solutions_archive"] = tname

        # Pareto Approximation (Pickled Dict)
        self.internal_storage["pareto_approximation"] = PickleUtils.pickleObjToStr(dict())
        # Solutions (Pickled list)
        self.internal_storage["solutions"] = PickleUtils.pickleObjToStr(list())

        # Need to create initial population's file (SMILES)
        self.internal_storage["population_file"] = ('population_%s.smi'
                                                    % (datetime.now().strftime("%Y%m%d_%H%M%S")))
        self.internal_storage["new_population_file"] = ('new_population_%s.smi'
                                                        % (datetime.now().strftime("%Y%m%d_%H%M%S")))
        # Solutions SMILES file
        solutions_file = ('solutions_%s.smi'
                          % (datetime.now().strftime("%Y%m%d_%H%M%S")))
        self.internal_storage["solutions_file"] = solutions_file

        # log file
        self.internal_storage["log"] = ('MOReactMolDesign_%s.log'
                                        % (datetime.now().strftime("%Y%m%d_%H%M%S")))
        # progress log file
        self.internal_storage["progress_log"] = ('progress_%s.log'
                                                 % (datetime.now().strftime("%Y%m%d_%H%M%S")))
        # figures directory
        self.internal_storage["fig_dir"] = ('MOReactMolDesign_visual_progress_%s'
                                            % (datetime.now().strftime("%Y%m%d_%H%M%S")))
        # other...

        # Debug info
        if self.debug:
            logger.debug("#######################################################")
            logger.debug("Input Params: %s\n" % pprint.pformat(params))
            logger.debug("Internal Storage: %s" % pprint.pformat(self.internal_storage))
            logger.debug("#######################################################")
        # end if

        return

    def _cleanUp(self):
        """Cleaning up..."""
        logger.info("Cleaning up...")
        # Remove temporary files
        os.unlink(self.internal_storage["archive"])
        os.unlink(self.internal_storage["elite_archive"])
        os.unlink(self.internal_storage["solutions_archive"])
        # Change to parent directory
        os.chdir("..")

        return

    def run_GP(self):
        """"""
        # TODO: Run GP algorithm
        results = DEAPGenProgMod.runGP(
            starting_molecules=copy.deepcopy(self.starting_molecules),
            transformations=copy.deepcopy(self.reactions),
            internal_storage=copy.deepcopy(self.internal_storage),
            debug=self.debug)

        # Cleanup...
        self._cleanUp()

        return
