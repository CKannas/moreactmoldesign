from __future__ import print_function

"""
Needed classes and functions for implementing and modifying functionality for 
DEAP Genetic Programing algorithm.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>
"""

__version__ = "0.1.0"

# Python Imports
import os
import sys
import logging
import pprint
import copyreg
from datetime import datetime
from concurrent import futures
import multiprocessing as mp
import types
import random
from inspect import isclass
import numpy as np
import pandas as pd
from pandas.plotting import parallel_coordinates
import operator  # Required for setting tree height limit
import progressbar

import copy
from functools import partial
import inspect

# Ploting Imports
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import networkx as nx

# DEAP Imports
from deap import gp
from deap import algorithms
from deap import base
from deap import creator
from deap import tools

# MOReactMolDesign Imports
from MOReactMolDesign import Config
from MOReactMolDesign.CLI_Tools.Support import FileParser
from MOReactMolDesign.Utilities import PickleUtils, \
                                       JSONUtils, \
                                       SubprocessUtils
from MOReactMolDesign.Optimizers import DesirabilityFuncs
from MOReactMolDesign.MolScorers import HBondCalc, \
                                        MolLogPCalc, \
                                        MRCalc, \
                                        RotBondsCalc, \
                                        TPSACalc, \
                                        MolWtCalc, \
                                        WaterSolCalc, \
                                        MolCost, \
                                        FPSimCalc

# RDKit Imports
from rdkit import Chem
from rdkit.Chem import Draw, \
                       PandasTools

random.seed(69)

# Global Variables
DEBUG = False
CPU_COUNT = mp.cpu_count()  # Utilise all available cores
# Errors List
errors = list()
# Internal Storage
int_store = dict()
# Transformations
transforms = None
reactionsDict = dict()
subsMatch = dict()
# Starting Ingredients
mols = dict()


# TODO: Refactor the code below to work with the new approach I'm trying to implement...
###############################################################################
#                           Utility Functions                                 #
###############################################################################
def _pickle_method(m):
    """Pickle instance method."""
    pprint.pprint(m)
    if getattr(m, "__self__", None) is None:
        return getattr, (m.__class__, m.__name__)
    else:
        return getattr, (m.__self__, m.__name__)
    # end if


# Register pickle method
copyreg.pickle(types.MethodType, _pickle_method)

# def _pickle_method(method):
#     """Pickle instance method.
#
#     :param method: Instance method to be pickled.
#     :returns:
#     """
#     func_name = method.__name__
#     obj = method.__self__
#     cls = method.__class__
#
#     print("\nBefore: ", func_name, obj, cls)
#     # deal with mangled names
#     if func_name.startswith('__') and not func_name.endswith('__'):
#         cls_name = cls.__name__.lstrip('_')
#         func_name = '_' + cls_name + func_name
#     # end if
#     print("After: ", func_name, obj, cls)
#
#     return _unpickle_method, (func_name, obj, cls)
#
#
# def _unpickle_method(func_name, obj, cls):
#     """UnPickle instance method.
#
#     :param func_name:
#     :param obj:
#     :param cls:
#     :returns:
#     """
#     print(cls.__mro__)
#     for cls in cls.__mro__:
#         try:
#             func = cls.__dict__[func_name]
#         except KeyError:
#             pass
#         else:
#             break
#         # end try-expect
#     # end for
#
#     return func.__get__(obj, cls)
#
#
# # Register pickle method
# copyreg.pickle(types.MethodType, _pickle_method, _unpickle_method)


def _zip(x, y):
    """Zip-like function for multiprocessing."""
    return x, y


def _search(_object, expr):
    """Return the value of an iterable that match to the expression.

    :param _object: An iterable object.
    :param expr: The expression.
    :returns: The key that match the given expr.
    """
    res = [k for k in _object if k.startswith(expr)]

    return res


def _scale(OldValue, OldMin, OldMax, NewMin=0, NewMax=1.0):
    """Scale a value between 0 and 1.

    :param OldValue: Raw value to scale.
    :param OldMin: Raw value's lower bound.
    :param OldMax: Raw value's upper bound.
    :param NewMin: Scaled value's lower bound. Defaults to 0.
    :param NewMax: Scaled value's upper bound. Default to 1.0.
    :returns: A scaled value in the range of NewMin to NewMax.
    """
    # If OldValue is NaN then replace it with 0...
    if np.isnan(OldValue):
        OldValue = 0.0
    # end if
    OldRange = (OldMax - OldMin)
    if OldRange == 0:
        NewValue = NewMin
    else:
        NewRange = (NewMax - NewMin)
        NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin
    # end if

    return NewValue


def _runTask(t_name, t_obj, t_cmd):
    """Runs a parallel task.

    :param t_name: Task name.
    :param t_cmd: Task command.
    :returns: Task results.
    """
    # 3.1. Task timer start
    task_start = datetime.now()
    # 3.2. Using SubprocessUtils run the scorer
    (status, output, err) = SubprocessUtils.runProcessRT(t_cmd,
                                                         enableStdout=DEBUG,
                                                         enableStderr=DEBUG)
    # 3.3. Task timer elapsed
    task_elapsed = datetime.now() - task_start
    if DEBUG:
        print("%s - Time elapsed: %s\n" % (t_name, task_elapsed))
    # end if

    return t_name, t_obj, status, output, err


###############################################################################
#        Class for performing Transformation Enumeration in GP tree           #
###############################################################################
class TransformEnum(object):
    """Transformation enumeration and product selection."""

    def __init__(self, transform, transform_name, prod_num=1):
        """Transformation enumeration and product selection.

        :param transform: The transformation as a ReactionContainer object.
        :param transform_name: Name of the transformation.
        :param prod_num: The index (starting from 1) of the product to select.
        """
        self.transform = transform
        self.transform_name = transform_name
        self.prod_num = prod_num - 1

        return

    def transformN(self, *smiles):
        """
        Perform the Transformation Enumeration.

        :param smiles: List of SMILES and or tuples of SMILES
                       and molecule object.
        :return: A tuple with the final product.
        """
        result = None

        if DEBUG:
            pprint.pprint("%s" % (self.transform.displayInfo()))
        # end if

        r_mols, mols_imgs = self._getMols(smiles)

        # check if transformation is enumerable
        if self.transform.IsEnumerate(r_mols):
            # Enumerate transformation
            self.transform.EnumerateReaction(r_mols)
            # Get all unique products
            p_mols = self.transform.getAllUniqueProducts()
            # Unpickle products to a list
            p_mols = [PickleUtils.unpickleStrToObj(pkl) for pkl in p_mols]
            if DEBUG:
                pprint.pprint("No. products: %d" % (len(p_mols)))
            # end if

            # convert to list
            r_mols = [Chem.RemoveHs(m[0]) for m in r_mols]
            if DEBUG:
                pprint.pprint("No. reactants: %d" % (len(r_mols)))
            # end if

            # Sanity check for sel_prod, if is out of index limits of the list
            # of products then set it to select the 1st product
            if self.prod_num < 0 or self.prod_num >= len(p_mols):
                self.prod_num = 0
            # end if
            # Select the product given by sel_prod number
            if len(p_mols):
                mol = p_mols[self.prod_num]
                mol_img = Draw.MolToImage(mol)
                mols_imgs.insert(0, mol_img)
                result = (Chem.MolToSmiles(mol, isomericSmiles=True),
                          mol,
                          mols_imgs)
            else:
                result = None
            # end if
        else:
            # Transformation not enumerable
            pprint.pprint("Transformation '%s' not enumerable!!!"
                          % self.transform_name)
        # end if

        return result

    def _getMols(self, smiles):
        """Get the molecules objects corresponding to the supplied SMILES

        :param smiles: List of SMILES and or tuples of SMILES
                       and molecule object.
        :returns: List of lists of molecule objects.
        """
        r_mols = list()
        mols_imgs = list()
        # For each input SMILES
        for smile in smiles:
            # If is a SMILES string...
            if isinstance(smile, str):
                # Find it in molsDict
                if DEBUG:
                    pprint.pprint("SMILES: %s" % smile)
                # end if

                if smile in mols:
                    r_mols.append([mols[smile]])
                    mols_imgs.append(Draw.MolToImage(mols[smile]))
                else:
                    r_mols.append([])
                # end if
                if DEBUG:
                    pprint.pprint("Reactant Molecules: %s" % pprint.pformat(r_mols))
                # end if
            # If is a tuple...
            elif isinstance(smile, tuple):
                r_mols.append([smile[1]])
                mols_imgs.extend(smile[2])
            # anything else (i.e. None)
            else:
                r_mols.append([])
            # end if
        # end for

        return r_mols, mols_imgs

###############################################################################


###############################################################################
#                     Modifications to DEAP functions                         #
###############################################################################
# Rewrite function generate to fit our needs
# This one is different than gp.generate
def generate(pset, min_, max_, condition, type_=None):
    """Generate a Tree as a list of list. The tree is build
    from the root to the leaves, and it stop growing when the
    condition is fulfilled.

    :param pset: Primitive set from which primitives are selected.
    :param min_: Minimum height of the produced trees.
    :param max_: Maximum Height of the produced trees.
    :param condition: The condition is a function that takes two arguments,
                      the height of the tree to build and the current
                      depth in the tree.
    :param type_: The type that should return the tree when called, when
                  :obj:`None` (default) the type of :pset: (pset.ret)
                  is assumed.
    :returns: A grown tree with leaves at possibly different depths
              depending on the condition function.
    """
    # Setup a logger
    logger = logging.getLogger("DEAP.GP.generate")

    if type_ is None:
        type_ = pset.ret
    # end if
    expr = []
    height = random.randint(min_, max_)
    # Stack values: depth, argument type and primitive index, argument index
    stack = [(0, type_, -1, -1)]
    while len(stack) != 0:
        depth, type_, prim_idx, arg_idx = stack.pop()
        if condition(height, depth):
            try:
                #
                prim_name = expr[prim_idx].name
                # get transformation
                trans_idx = reactionsDict[prim_name][0]
                trans = transforms.reactions[trans_idx]
                # find matching compounds
                smarts = Chem.MolToSmarts(
                    trans.rxn.GetReactantTemplate(arg_idx))
                matched_mols = subsMatch[smarts]
                # debug inform
                if DEBUG:
                    logger.debug("Primitive: %s\n"
                                 "\tTransform Id: %s\n"
                                 "\tReactant Idx: %s Reactant SMARTS: %s\n"
                                 "\tMatching Molecules: %s"
                                 % (prim_name, trans_idx, arg_idx, smarts,
                                    pprint.pformat(matched_mols)))
                    # end if
                terms_list = list()
                for smi in matched_mols:
                    for term in pset.terminals[type_]:
                        if repr(smi) == term.name:
                            terms_list.append(term)
                        # end if
                    # end for
                # end for
                # Empty matched terms list then perform random choice
                if len(terms_list) == 0:
                    terms_list = pset.terminals[type_]
                # end if
                if DEBUG:
                    logger.debug("Matched Terminals: %s\n"
                                 % pprint.pformat(terms_list))
                # end if
                #term = random.choice(pset.terminals[type_])
                term = random.choice(terms_list)
            except IndexError:
                _, _, traceback = sys.exc_info()
                raise(IndexError, "The gp.generate function tried to add "
                                  "a terminal of type '%s', but there is "
                                  "none available." % (type_,), traceback)
            # end try-except
            if isclass(term):
                term = term()
            # end if
            expr.append(term)
        else:
            try:
                prims_list = list()
                if prim_idx >= 0:
                    prim_name = expr[prim_idx].name
                    # get transformation
                    trans_idx = reactionsDict[prim_name][0]
                    trans = transforms.reactions[trans_idx]
                    # get pattern
                    pattern = trans.rxn.GetReactantTemplate(arg_idx)
                    #
                    if DEBUG:
                        logger.debug("Primitive Idx: %s "
                                     "Number of Reactants: %s\n"
                                     "Expression %s"
                                     % (prim_idx, trans.numReactants, expr))
                    # end if
                    # Need to find a transformation that will match
                    # the corresponding reactant of the previous transformation
                    if DEBUG:
                        logger.debug(">>> Transformation as reactant. <<<")
                    # end if
                    for p in pset.primitives[type_]:
                        # get transformation
                        p_name = p.name
                        t_idx = reactionsDict[p_name][0]
                        t = transforms.reactions[t_idx]
                        # get product
                        prod = t.rxn.GetProductTemplate(0)
                        if DEBUG:
                            logger.debug("%s <- %s %s"
                                         % (Chem.MolToSmarts(pattern),
                                            Chem.MolToSmiles(prod),
                                            Chem.MolToSmarts(prod)))
                        # end if
                        try:
                            prod = Chem.MolFromSmiles(Chem.MolToSmiles(prod))
                            prod = Chem.AddHs(prod)
                            if DEBUG:
                                logger.debug("%s %s" % (Chem.MolToSmiles(prod),
                                                        Chem.MolToSmarts(prod)))
                            # end if
                        except:
                            prod = t.rxn.GetProductTemplate(0)
                            if DEBUG:
                                logger.debug("%s %s" % (Chem.MolToSmiles(prod),
                                                        Chem.MolToSmarts(prod)))
                            # end if
                        # end try-except
                        try:
                            if prod and prod.HasSubstructMatch(pattern):
                                prims_list.append(p)
                            # end if
                        except:
                            pass
                        # end try-except
                    # end for
                    if DEBUG:
                        logger.debug("Primitives List: %s"
                                     % pprint.pformat(prims_list))
                    # end if
                # end if
                # If not found any matching primitive then perform
                # a random choice
                if len(prims_list) == 0:
                    prims_list = pset.primitives[type_]
                # end if
                prim = random.choice(prims_list)
            except IndexError:
                _, _, traceback = sys.exc_info()
                raise(IndexError, "The gp.generate function tried to add "
                                  "a primitive of type '%s', but there is "
                                  "none available." % (type_,), traceback)
            # end try-except
            expr.append(prim)
            # get primitive index
            prim_idx = len(expr) - 1
            # Argument index
            arg_idx = len(prim.args) - 1
            for arg in reversed(prim.args):
                stack.append((depth + 1, arg, prim_idx, arg_idx))
                arg_idx -= 1
            # end for
        # end if
    # end while

    return expr


# set gp.generate to generate
gp.generate = generate


def initPrimitiveSet():
    """Initialize Primitives and Terminals Sets for the Genetic Programming (GP).

    :returns: The primitive set.
    """
    # Setup Primitives and Terminals Sets for the Genetic Programming (GP)
    pset = gp.PrimitiveSet("MAIN", 0)
    # 1. Terminals set
    for smi in mols:
        smi = repr(smi)
        pset.addTerminal(smi, name=smi)
    # end for

    # 2. Primitives Set
    # 2.1. Get useful information from reactions to a dictionary
    # global reactionsDict
    # reactionsDict = dict()
    for idx, reaction in enumerate(transforms.reactions):
        te = TransformEnum(reaction, reaction.name, 1)
        in_patterns = [Chem.MolToSmarts(reaction.rxn.GetReactantTemplate(i))
                       for i in range(reaction.numReactants)]
        out_patterns = [Chem.MolToSmarts(reaction.rxn.GetProductTemplate(i))
                        for i in range(reaction.numProducts)]
        reactionsDict[reaction.name] = (idx,
                                        reaction.numReactants,
                                        te,
                                        in_patterns,
                                        out_patterns)
    # end for

    # 2.2. Add primitives
    for r_name, r_info in reactionsDict.items():
        te = r_info[2]
        # Create a primitive according to the number of reactants
        if r_info[1] == 1:
            pset.addPrimitive(te.transformN, 1, name=r_name)
        elif r_info[1] == 2:
            pset.addPrimitive(te.transformN, 2, name=r_name)
        elif r_info[1] == 3:
            pset.addPrimitive(te.transformN, 3, name=r_name)
            # end if
    # end for

    # 2.3. Rename Functions arguments
    pset.renameArguments(ARG0="mol")

    return pset
###############################################################################


# TODO: Parallel Version...
def initPopulation(pop_container, indi_init, pset, n, logger=None):
    """Generate a valid population for the Genetic Programming Algorithm.

    :param pop_container: The type of the object of the generated population.
    :param indi_init: The function that initializes individuals.
    :param pset: The primitive set of the Genetic Programming Algorithm.
    :param n: The number of individuals to be generated.
    :param logger: Handle to a logger function.
    :returns: A container with n individuals, to be used as a starting
              population for the Genetic Programming Algorithm.
    """
    # Generate at least n valid individuals
    tmp_list = list()
    duplicates = dict()
    valid = 0
    invalid = 0
    while valid < n:
        # 1. Generate n individuals
        individuals = [indi_init() for _ in range(n)]

        with futures.ProcessPoolExecutor(CPU_COUNT) as executor:
            tmp_pickled_objs = executor.map(PickleUtils.pickleObjToStr,
                                            individuals)
            for pickled_obj in tmp_pickled_objs:
                pprint.pprint(pickled_obj)
            # end for
        # end with

        sys.exit(-1)

        ########################################################################
        # # 2. Compile the generated individuals in serial
        # individuals_results = map(gp.compile,
        #                           individuals,
        #                           (pset for _ in range(n)))
        # # Debug info
        # if DEBUG and logger:
        #     logger.debug("Generation of individuals completed.")
        #     logger.debug("Individuals: %d Results: %s"
        #                  % (len(individuals), type(individuals_results)))
        # # end if
        #
        # # 3. Zip individuals with their results
        # results = zip(individuals, individuals_results)
        #
        # # 4. Check if the individuals are valid or invalid
        # for indi, result in results:
        #     s = str(indi)
        #     # Check if an individual has been generated previously
        #     if s not in duplicates:
        #         duplicates[s] = 1
        #         indi.result = result
        #         # Keep only valid individuals
        #         if result:
        #             valid += 1
        #             tmp_list.append(indi)
        #         else:
        #             invalid += 1
        #             # end if
        #     else:
        #         duplicates[s] += 1
        #     # end if
        #     if DEBUG and logger:
        #         logger.debug("Valid: %d Invalid %d" % (valid, invalid))
        #     # end if
        #     # When enough (at least n) valid individuals are generated stop
        #     if valid >= n:
        #         break
        #         # end if
        # # end for
        # if logger:
        #     logger.info("Valid: %d Invalid %d" % (valid, invalid))
        # # end if
        ########################################################################
        # 2. Utilize multi-core parallelism to
        # generate and check if generated individuals are valid
        with futures.ProcessPoolExecutor(CPU_COUNT) as executor:
            # Compile the generated individuals in parallel
            individuals_results = executor.map(gp.compile,
                                               individuals,
                                               (pset for _ in range(n)))
            # Debug info
            if DEBUG and logger:
                logger.debug("Generation of individuals completed.")
                logger.debug("Individuals: %d Results: %s"
                             % (len(individuals), type(individuals_results)))
                for res in individuals_results:
                    print(type(res))
                # end for
            # end if

            results = executor.map(_zip, individuals, individuals_results)
            # Debug info
            if DEBUG and logger:
                logger.debug("Zip individuals completed.")
            # end if

            # 2.1. Check if the individuals are valid or invalid
            for indi, result in results:
                s = str(indi)
                # Check if an individual has been generated previously
                if s not in duplicates:
                    duplicates[s] = 1
                    indi.result = result
                    # Keep only valid individuals
                    if result:
                        valid += 1
                        tmp_list.append(indi)
                    else:
                        invalid += 1
                    # end if
                else:
                    duplicates[s] += 1
                # end if
                if DEBUG and logger:
                    logger.debug("Valid: %d Invalid %d" % (valid, invalid))
                # end if
                # When enough (at least n) valid individuals are generated stop
                if valid >= n:
                    break
                # end if
            # end for
            if logger:
                logger.info("Valid: %d Invalid %d" % (valid, invalid))
            # end if
        # end with
        ########################################################################
    # end while
    if logger:
        logger.info("Population initialisation: %d valid "
                    "and %d invalid individuals generated"
                    % (valid, invalid))
        logger.info("Population initialisation: %d duplicate "
                    "individuals generated"
                    % (sum(duplicates.values())))
    # end if

    return pop_container(tmp_list)
################################################################################


# TODO: Scorers initialization method.
def initScorers(logger):
    """Initialize scorers/models instances using information supplied.

    :param logger: Handler for logger.
    """
    # Debug info
    if DEBUG:
        logger.debug("Initialising Scorers Conditions")
        logger.debug("===============================")
    # end if
    # 1. For each objective in the input find supporting information
    for obj in int_store["objectives"]:
        tmp_scorer = obj.copy()
        # 1.1. Get property label
        for obj_q in int_store["quantities"]:
            # print obj_q
            if obj["id"] == obj_q["id"]:
                # objective label
                tmp_scorer["label"] = obj_q["label"]
                break
            # end if
        # end for
        # 1.2. Get information about the model that calculates this objective
        # model found
        model_found = False
        for model in int_store["models"]:
            for prop in model["calculates"]:
                if obj["id"] == prop["id"]:
                    # model id
                    tmp_scorer["model_id"] = model["id"]
                    # model label
                    tmp_scorer["model_label"] = model["label"]
                    # model invocation
                    tmp_scorer["invocation"] = model["invocation"]
                    # model name
                    tmp_scorer["model_name"] = model["invocation"]["invokerData"]["pythonClass"]
                    # model found
                    model_found = True
                    break
                # end if
            # end for
            # Get input conditions
            # Get overwritten conditions ids
            if "conditions" in obj:
                obj_conditions_ids = [cond["id"] for cond in obj["conditions"]]
            else:
                obj_conditions_ids = list()
                tmp_scorer["conditions"] = list()
            # end if
            for cond in model["inputs"]:
                if cond["id"] not in obj_conditions_ids:
                    tmp_scorer["conditions"].append(cond)
                # end if
            # end for
            # if model info has been added then break
            if model_found:
                break
            # end if
        # end for
        # 1.3. Get conditions values
        for cond in int_store["conditions"]:
            # For each input condition
            for in_cond in tmp_scorer["conditions"]:
                # if there is not a value attached to it
                # then attach a default value
                if cond["id"] == in_cond["id"] and "value" not in in_cond:
                    in_cond["value"] = cond["value"]
                    break
                # end if
            # end for
        # end For
        # Debug info
        if DEBUG:
            logger.debug("Scorer Information: %s" % pprint.pformat(tmp_scorer))
        # end if
        # 1.4. Add to scorers list
        int_store["scorers"].append(tmp_scorer)
    # end for
    # Debug info
    if DEBUG:
        logger.debug("Scorers: %s" % pprint.pformat(int_store["scorers"]))
    # end if

    # Debug info
    if DEBUG:
        logger.debug("Initialising Scorers")
        logger.debug("====================")
    # end if
    # 2. Initialise each scorer that is needed
    for scorer in int_store["scorers"]:
        scorer_name = scorer["model_name"]
        # Debug info
        if DEBUG:
            logger.debug("Scorer Name: %s" % scorer_name)
        # end if
        # 2.1. Initialise RDKit Property Calculators
        # 2.1.1 Molecular Weight
        if scorer_name == "RDKitMolWtCalculator":
            MolWt = MolWtCalc.MolWtCalc(DEBUG)
            scorer["handler"] = MolWt
            scorer["objective_property"] = MolWt.obj_name
        # 2.1.2. Hydrogen Bond Donors
        elif scorer_name == "RDKitNHBDCalculator":
            NumHBD = HBondCalc.HBondDonorsCalc(DEBUG)
            scorer["handler"] = NumHBD
            scorer["objective_property"] = NumHBD.obj_name
        # 2.1.3. Hydrogen Bond Acceptors
        elif scorer_name == "RDKitNHBACalculator":
            NumHBA = HBondCalc.HBondAcceptorsCalc(DEBUG)
            scorer["handler"] = NumHBA
            scorer["objective_property"] = NumHBA.obj_name
        # 2.1.4. Topological Polar Surface Area
        elif scorer_name == "RDKitTPSACalculator":
            TPSA = TPSACalc.TPSACalc(DEBUG)
            scorer["handler"] = TPSA
            scorer["objective_property"] = TPSA.obj_name
        # 2.1.5. Rotatable Bonds
        elif scorer_name == "RDKitRotBondsCalculator":
            RotBonds = RotBondsCalc.RotBondCalc(DEBUG)
            scorer["handler"] = RotBonds
            scorer["objective_property"] = RotBonds.obj_name
        # 2.1.6. LogP
        elif scorer_name == "RDKitLogPCalculator":
            cLogP = MolLogPCalc.MolLogPCalc(DEBUG)
            scorer["handler"] = cLogP
            scorer["objective_property"] = cLogP.obj_name
        # 2.1.7. Molecular Refractivity
        elif scorer_name == "RDKitMRCalculator":
            MR = MRCalc.MolRefractCalc(DEBUG)
            scorer["handler"] = MR
            scorer["objective_property"] = MR.obj_name
        # 2.2. Initialise Water Solubility Calculator
        elif scorer_name == "WaterSolubilityCalculator":
            WSCalc = WaterSolCalc.ESOL(DEBUG)
            scorer["handler"] = WSCalc
            scorer["objective_property"] = WSCalc.obj_name
        # 2.3. Initialise Substance Cost Calculator
        elif scorer_name == "SubstanceCostCalculator":
            # Create a dictionary for the reactions cost
            # For each reaction get its reaction id and cost
            reactCost = dict()
            for reaction in transforms.reactions:
                reactCost[reaction.reactionId] = reaction.getProperty("Cost", 0)
            # end for
            SubstanceCost = MolCost.MolCost(reactionsCost=reactCost,
                                            debug=DEBUG)
            scorer["handler"] = SubstanceCost
            scorer["objective_property"] = SubstanceCost.obj_name
        # 2.4. Initialise Fingerprint Similarity Calculator
        elif scorer_name == "FingerprintSimilarityCalculator":
            # TODO: Set the data in params file and then initialise the scorer here.
            pass
        # 2.5. Unidentified scorer/model
        else:
            logger.warning("Error: Unidentified scorer/model name: %s"
                           % scorer_name)
        # end if
        # 2.5. Save objective property name in the correct objective
        for obj in int_store["objectives"]:
            if obj["id"] == scorer["id"]:
                obj["objective_property"] = scorer["objective_property"]
                obj["label"] = scorer["label"]
                break
            # end if
        # end for
        # Debug info
        if DEBUG:
            logger.debug("Scorer Information: %s" % pprint.pformat(scorer))
        # end if
    # end for

    return


def multiScoring(population, logger):
    """Multiple scoring execution. Updates input with scoring results.

    :param population: List of individuals for scoring.
    :param logger: Handler for logger.
    """
    # Get the molecules from the population
    tmp_mols = [indi.result[1] for indi in population]
    assert len(tmp_mols) == len(population)

    # TODO: We will probably need to do it in parallel in the future...
    # Call each scorer to calculate the requested property
    for scorer in int_store["scorers"]:
        # inform
        logger.info("Running %s..." % scorer["model_label"])
        # Call scorer's handler...
        fit_scores = scorer["handler"].calcFitness(tmp_mols)
        assert len(fit_scores) == len(tmp_mols)
    # end for

    assert len(tmp_mols) == len(population)
    # Restore molecules to population...
    for idx, indi in enumerate(population):
        # Convert individual's result to mutable list
        tmp_result = list(indi.result)
        # Get the matching molecule, using index
        tmp_result[1] = tmp_mols[idx]
        # convert to immutable tuple
        indi.result = tuple(tmp_result)
    # end for

    return


def initFitnessWeights():
    """Initialize and fitness's weight.

    :returns: A tuple of the fitness's weights.
    """
    return tuple([1.0 for _ in int_store["objectives"]])


def evalIndividual(individual):
    """Evaluate an individual of the Genetic Programming algorithm.

    :param individual: The result of an individual.
    :returns: A tuple with the fitness values of the individual.
    """
    fitness_values = list()
    mol = individual.result[1]
    # evaluate...
    for obj in int_store["objectives"]:
        # Skip objective that does not have an objective_property field
        if "objective_property" not in obj:
            fitness_values.append(float("inf"))
        else:
            criteria_name = obj["objective_property"]
            if mol.HasProp(criteria_name):
                raw_value = float(mol.GetProp(criteria_name))
                # print(obj["criteria"]["functor"])
                # Use the correct desirability function
                if obj["criteria"]["functor"] == "lessthan":
                    # print obj["criteria"]["values"]
                    # Less than
                    threshold = obj["criteria"]["values"][0]
                    fit_value = DesirabilityFuncs.minimizeD(raw_value,
                                                            Upper=threshold)
                elif obj["criteria"]["functor"] == "morethan":
                    # print(obj["criteria"]["values"])
                    # More than
                    threshold = obj["criteria"]["values"][0]
                    fit_value = DesirabilityFuncs.maximizeD(raw_value,
                                                            Low=threshold)
                elif obj["criteria"]["functor"] == "smallaspossible":
                    # print("\n")
                    # Small as possible
                    fit_value = DesirabilityFuncs.minimizeD(raw_value)
                elif obj["criteria"]["functor"] == "largeaspossible":
                    # print("\n")
                    # Large as possible
                    fit_value = DesirabilityFuncs.maximizeD(raw_value)
                elif obj["criteria"]["functor"] == "between":
                    # print(obj["criteria"]["values"])
                    # Between - Range
                    low_thresh = obj["criteria"]["values"][0]
                    upper_thresh = obj["criteria"]["values"][1]
                    fit_value = DesirabilityFuncs.rangeD(raw_value,
                                                         Low=low_thresh,
                                                         Upper=upper_thresh)
                else:
                    fit_value = raw_value
                # end if
                fitness_values.append(fit_value)
            else:
                fitness_values.append(float("inf"))
            # end if
        # end if
    # end for

    return tuple(fitness_values)
################################################################################


def subStructureMatching(logger=None):
    """Associate molecules to transformation's reactants via substructure matching.

    :param logger: Logger handler.
    :returns: A dictionary with the matching molecules per substructure pattern.
    """
    tmp_subsMatch = dict()
    # for each transformation
    for tran in transforms.reactions:
        # For each reactant pattern
        for react_idx in range(tran.numReactants):
            pattern = tran.rxn.GetReactantTemplate(react_idx)
            smarts = Chem.MolToSmarts(pattern)
            tmp_subsMatch[smarts] = list()
            # Find matching molecules
            for smiles, mol in mols.items():
                try:
                    molH = Chem.AddHs(mol)
                except:
                    pass
                finally:
                    if molH.HasSubstructMatch(pattern):
                        tmp_subsMatch[smarts].append(smiles)
                    # end if
                    # debug
                    if DEBUG and logger:
                        logger.debug("Transf. Id: %s Pattern: %s SMILES: %s"
                                     % (tran.reactionId, smarts, smiles))
                    # end if
                # end try-except
            # end for
        # end for
    # end for

    return tmp_subsMatch


def findSame(indi_1, indi_2):
    """Check two individuals and return True if they are the same,
    False otherwise.

    :param indi_1: First individual.
    :param indi_2: Second individual.
    :returns: True if same, False otherwise.
    """
    if((indi_1.result[0] == indi_2.result[0]) and
       (str(indi_1) == str(indi_2))):
        return True
    # end if

    return False

################################################################################
#                            Update Archives                                   #
################################################################################
def updateArchive(population, logger):
    """Update archive with current population.

    :param population: Population to archive.
    :param logger: Handler for logger.
    """
    # Update progress archive
    curr_iter = int_store["curr_iter"]
    with open(int_store["archive"], "rb") as f:
        archive = PickleUtils.unpickleFileToObj(f)
    # end with
    if curr_iter not in archive:
        archive[curr_iter] = dict()
    # end if
    archive[curr_iter] = population

    if DEBUG:
        logger.debug("Archive Size: %d\tLatest Entry: %d"
                     % (len(archive), len(archive[curr_iter])))
    # end if

    with open(int_store["archive"], "wb") as f:
        PickleUtils.pickleObjToFile(archive, f)
    # end with

    return


def updateEliteArchive(population, logger):
    """Reset and update elite archive.

    :param population: Population.
    :param logger: Handler for logger.
    """
    elite_archive = dict()
    for indi in population:
        smiles = indi.result[0]
        s = str(indi)
        elite_archive[(smiles, s)] = indi
    # end for

    logger.info("Elite Archive: %d" % len(elite_archive))

    with open(int_store["elite_archive"], "wb") as f:
        PickleUtils.pickleObjToFile(elite_archive, f)
    # end with

    return


def getEliteArchive(population, logger):
    """Get individuals from elite archive and update population.

    :param population: The population
    :param logger: Handler for logger.
    :returns: Updated population.
    """
    with open(int_store["elite_archive"], "rb") as f:
        elite_archive = PickleUtils.unpickleFileToObj(f)
    # end with

    logger.info("Elite Archive: %d" % len(elite_archive))

    for indi_id in elite_archive:
        indi = elite_archive[indi_id]
        pop_id = [(i.result[0], str(i)) for i in population]
        if indi_id not in pop_id:
            population.append(indi)
        # end if
    # end for

    return population


def updateSolutionsArchive(solutions, logger):
    """Update surfactant archive.

    :param solutions: List of individuals.
    :param logger: Handler for logger.
    """
    with open(int_store["solutions_archive"], "rb") as f:
        archive = PickleUtils.unpickleFileToObj(f)
    # end with

    logger.info("Surfactants Archive: %d" % len(archive))

    for solution in solutions:
        s = str(solution)
        key = (solution.result[0], s)
        if key not in archive:
            archive[key] = solution
        # end if
    # end for
    with open(int_store["solutions_archive"], "wb") as f:
        PickleUtils.pickleObjToFile(archive, f)
    # end with

    logger.info("Surfactants Archive: %d" % len(archive))

    return


###############################################################################
#                            Final Solutions                                  #
###############################################################################
def getFinalSolutions(population, logger=None):
    """Generate a files for the final solutions.

    :param population: Last population.
    :param logger: Logger handler.
    :returns: The file paths for each file that is created.
    """
    # Final products
    final_prods = [indi.result[1] for indi in population]

    # Create history images...

    # List of available molecule properties names
    molProps = ["History"]
    for objective in int_store["objectives"]:
        if "objective_property" not in objective:
            continue
        obj_name = objective["objective_property"]
        molProps.append(obj_name)
    # end for
    molProps.append("NumTransforms")

    # Final solutions SMILES files
    fs_smiles_file = ('final_solutions_%s.smi'
                      % (datetime.now().strftime("%Y%m%d_%H%M%S")))
    FileParser.SMILESFileWriter(fs_smiles_file, final_prods, molProps)
    #
    # Save data to a dictionary
    data = dict()
    data["Name"] = list()
    data["Mol"] = list()
    data["SMILES"] = list()
    data["NumTransforms"] = list()
    for objective in int_store["objectives"]:
        if "objective_property" not in objective:
            continue
        obj_name = objective["objective_property"]
        data[obj_name] = list()
    # end for
    for indi in population:
        smiles, mol, imgs = indi.result
        data["Name"].append(mol.GetProp("_Name"))
        data["Mol"].append(mol)
        data["SMILES"].append(smiles)
        for objective in int_store["objectives"]:
            if "objective_property" not in objective:
                continue
            obj_name = objective["objective_property"]
            try:
                value = eval(mol.GetProp(obj_name))
            except(NameError, SyntaxError):
                value = mol.GetProp(obj_name)
            finally:
                data[obj_name].append(value)
            # end try-except
        # end for
        data["NumTransforms"].append(mol.GetProp("NumTransforms"))
    # end for

    # Change the name of the objective to have the objective label
    objs_cols = list()
    for objective in int_store["objectives"]:
        if "objective_property" not in objective:
            continue
        # end if
        obj_name = objective["objective_property"]
        if "label" in objective:
            new_name = objective["label"] + " (" + obj_name + ")"
            new_name = new_name.decode("utf-8")
            objs_cols.append(new_name)
            data[new_name] = data[obj_name]
            del data[obj_name]
        else:
            objs_cols.append(obj_name)
        # end if
    # end for
    objs_cols.append("NumTransforms")

    # Convert to RDKit enabled Pandas dataframe
    dataRDkitDF = PandasTools.pd.DataFrame(data)
    # Remove duplicates
    dataRDkitDF = dataRDkitDF.drop_duplicates("SMILES")
    # Make index start from 1
    dataRDkitDF.index += 1

    # Save image file
    img_file = "final_solutions_molgrid.png"
    img = PandasTools.FrameToGridImage(dataRDkitDF,
                                       column="Mol",
                                       legendsCol="Name",
                                       molsPerRow=5)
    img.save(img_file)

    # Save to Excel file
    # Final solutions Excel file
    excel_file = ('final_solutions_%s.xlsx'
                  % (datetime.now().strftime("%Y%m%d_%H%M%S")))
    #
    cols = ["SMILES", "Name"] + objs_cols
    with pd.ExcelWriter(excel_file, engine='xlsxwriter') as writer:
        dataRDkitDF.to_excel(writer,
                             sheet_name='Final_Solutions',
                             index=False,
                             columns=cols)
    # end with

    # Save HTML
    html_file = ('final_solutions_%s.html'
                 % (datetime.now().strftime("%Y%m%d_%H%M%S")))
    cols = ["SMILES", "Name", "Mol"] + objs_cols
    with open(html_file, "w") as fd:
        # HTML Header
        html_header = "<head><meta charset=\"UTF-8\"></head>"
        fd.write(html_header)
        # Dataframe
        html_data = dataRDkitDF.to_html(index=True,
                                        columns=cols,
                                        escape=False)
        fd.write(html_data.encode("utf-8"))
    # end with

    # Parallel Coordinates Figure
    # plotParallelCoords(population, logger=logger)

    return fs_smiles_file, img_file, excel_file, html_file


###############################################################################
#                           Plotting Functions                                #
###############################################################################
def plotParallelCoords(solutions, iteration=-1, logger=None):
    """Plot parallel coordinates of the solutions.

    :param solutions: Solutions to visualise their objectives in
                      parallel coordinates.
    :param iteration: Current iteration count.
    :param logger: Logger handler.
    """
    # Do nothing if solutions is empty...
    if not solutions:
        return
    # end if

    # Save data to be plotted in a dictionary
    data = dict()
    data["Name"] = list()
    for objective in int_store["objectives"]:
        if "objective_property" not in objective:
            continue
        obj_name = objective["objective_property"]
        data[obj_name] = list()
    # end for
    # Save molecule objective properties values in the dictionary
    for indi in solutions:
        # Get smiles and the molecule object from the individual
        smiles, mol, imgs = indi.result
        data["Name"].append(smiles)
        # data["Name"].append(mol.GetProp("_Name"))
        for objective in int_store["objectives"]:
            # Skip if there is not an objective property
            if "objective_property" not in objective:
                continue
            # end if
            obj_name = objective["objective_property"]
            minValue = objective["Min"]
            maxValue = objective["Max"]
            # Check if the objective property is available
            if mol.HasProp(obj_name):
                # Scale objective values between 0 and 1
                raw_value = float(mol.GetProp(obj_name))
                scaled_value = _scale(raw_value,
                                      OldMin=minValue,
                                      OldMax=maxValue)
            else:
                # Property not available set scaled value to 1.0...
                scaled_value = 1.0
            # end if
            data[obj_name].append(scaled_value)
        # end for
    # end for

    # Convert dictionary to data frame
    data_DF = pd.DataFrame(data)

    # Plot parallel coordinates and save them in image file
    fig_dir = int_store["fig_dir"]
    # If the directory does not exist, create it
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    # end if
    if iteration >= 0:
        figname = ("par_coords_iter_%s.png" % (iteration))
        title = ("MOST Solutions Objectives values @ iteration %s"
                 % iteration)
    else:
        figname = "par_coords_final.png"
        title = "MOST Final Solutions Objectives values"
    # end if
    figpath = os.path.join(fig_dir, figname)

    # Plotting
    plt.figure(figsize=(24, 14), dpi=300)
    ax = plt.subplot(111)
    # Plot title
    ax.set_title(title)
    # Y-axis label
    ax.set_ylabel('Objectives values')
    parallel_coordinates(data_DF, "Name")
    # Shrink current axis's height by 10% on the bottom
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,
                     box.width, box.height * 0.9])
    # Put a legend below current axis
    lgd = ax.legend(loc='upper center',
                    bbox_to_anchor=(0.5, -0.05),
                    fancybox=True,
                    shadow=True,
                    ncol=4)
    # Save figure
    plt.savefig(figpath,
                dpi=300,
                format='png',
                bbox_extra_artists=(lgd,),
                bbox_inches='tight')
    plt.close()

    # info
    if logger:
        logger.info("Parallel coordinates plot saved at: %s" % (figpath))
    # end if

    return


def plotSolutions(solutions, logger=None):
    """Plot an image for each individual in **solutions***,
    and save it to a file.

    :param solutions: List of individuals.
    :param logger: Logger handler.
    """
    # Do nothing if solutions is empty...
    if not solutions:
        return
    # end if

    # Plot solutions trees and save them in image file
    fig_dir = int_store["fig_dir"]
    # If the directory does not exist, create it
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    # end if

    # Create an image plot for each individuals
    for idx, indi in enumerate(solutions):
        # Get molecules images
        imgs = indi.result[2]

        # Generate a graph from the individual tree
        nodes, edges, labels = gp.graph(indi)
        g = nx.Graph()
        g.add_nodes_from(nodes)
        g.add_edges_from(edges)
        pos = nx.nx_pydot.graphviz_layout(g, prog="dot")

        # Keep only primitive nodes labels, i.e. transformations
        prim_labels = dict()
        for l in labels:
            label = ""
            if isinstance(indi[l], gp.Primitive):
                # Replace "_" with " " and
                # capitalise first letter of each word
                label = labels[l].replace("_", " ").title()
            # end if
            prim_labels[l] = label
        # end for

        # There are two plots here:
        # First one for the graph
        # Second one for the corresponding node images
        fig = plt.figure(1, dpi=300)
        ax = plt.subplot(111)
        # Remove figure axis
        plt.axis('off')

        # Draw graph edges
        nx.draw_networkx_edges(g, pos)

        trans = ax.transData.transform
        trans2 = fig.transFigure.inverted().transform

        cut = 1.00
        # X-axis limits
        xmin = cut * min(xx for xx, yy in pos.values())
        xmax = cut * max(xx for xx, yy in pos.values())
        # Y-axis limits
        ymin = cut * min(yy for xx, yy in pos.values())
        ymax = cut * max(yy for xx, yy in pos.values())

        # print xmin, xmax, xmin + xmax
        # print ymin, ymax, ymin + ymax

        plt.xlim(0, xmin + xmax)
        plt.ylim(0, ymin + ymax)

        # height and width of the image
        h = 37.0
        w = 37.0
        # For each node draw a new plot from the molecule image
        for each_node in g:
            # figure coordinates
            xx, yy = trans(pos[each_node])
            # axes coordinates
            xa, ya = trans2((xx, yy))

            # this is the image size
            # piesize_1 = (300.0 / (h*80))
            piesize_1 = (300.0 / (h * h))
            # piesize_2 = (300.0 / (w*80))
            piesize_2 = (300.0 / (w * w))
            p2_2 = piesize_2 / 2
            p2_1 = piesize_1 / 2
            a = plt.axes([xa - p2_2, ya - p2_1, piesize_2, piesize_1])

            # Set x-axis label
            a.set_xlabel(prim_labels[each_node])

            # Get image
            img = imgs[each_node]
            # Insert image into the node
            g.node[each_node]['image'] = img
            # Display it
            a.imshow(g.node[each_node]['image'])
            # Turn off the axis from minor plot
            # a.axis('off')
            a.set_xticks([])
            a.set_yticks([])
        # end for
        # fig.tight_layout()
        # plt.axis("off")
        # plt.show()

        if DEBUG:
            fig_size = fig.get_size_inches()
            logger.debug("Figure Size: %s (inches) %s (pixels)"
                         % (fig_size, fig_size * fig.dpi))
        # end if

        # Save figure
        figname = ("solution_%s.png" % idx)
        figpath = os.path.join(fig_dir, figname)
        plt.savefig(figpath,
                    dpi=300,
                    format='png',
                    bbox_inches='tight')
        plt.close()
        # info
        if logger:
            logger.info("Solution's %d plot saved at: %s" % (idx, figpath))
        # end if
    # end for

    return


def plotGenealogy(history, toolbox, logger=None):
    """Plot genealogy image from **history** handler.

    :param history: History handler.
    :param toolbox: Toolbox handler.
    :param logger: Logger handler.
    """

    graph = nx.DiGraph(history.genealogy_tree)
    graph = graph.reverse()     # Make the grah top-down
    colors = [toolbox.evaluate(history.genealogy_history[i])[0] for i in graph]
    labels = {i: str(i) for i in graph}

    # 8k UHDTV Super Hi-Vision resolution
    fig = plt.figure(figsize=(26, 15), dpi=300)

    pos = nx.nx_pydot.graphviz_layout(graph,
                                      prog="dot",
                                      args='-Gratio=auto -Gsize="26,15"')
    nx.draw_networkx_nodes(graph, pos, node_color=colors)
    nx.draw_networkx_edges(graph, pos)
    nx.draw_networkx_labels(graph, pos, labels, font_size=10)

    if DEBUG:
        fig_size = fig.get_size_inches()
        logger.debug("Figure Size: %s (inches) %s (pixels)"
                     % (fig_size, fig_size * fig.dpi))
    # end if

    fig.tight_layout()

    plt.axis('off')
    # plt.show()

    # Plots folder
    fig_dir = int_store["fig_dir"]
    # If the directory does not exist, create it
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    # end if

    # Save figure
    figname = "history_genealogy.png"
    figpath = os.path.join(fig_dir, figname)
    plt.savefig(figpath,
                dpi=300,
                format='png',
                bbox_inches='tight')
    plt.close()
    # info
    if logger:
        logger.info("History genealogy plot saved at: %s" % figpath)
    # end if

    return


###############################################################################
#             Chemical Diverse Selection by Pareto Ranking                    #
###############################################################################
def divSelect(individuals, k, nd="standard", logger=None):
    """Apply a chemical/objective space diversity selection operator on
    the *individuals*. Usually, the size of *individuals* will be larger
    than *k* because any individual present in *individuals* will appear in
    the returned list at most once. Having the size of *individuals* equals
    to *k* will have no effect other than sorting the population according to
    their front rank. The list returned contains references to the input
    *individuals*.

    :param individuals: A list of individuals to select from.
    :param k: The number of individuals to select.
    :param nd: Specify the non-dominated algorithm to use: 'standard' or 'log'.
    :param logger: Handler to a logger.
    :returns: A list of selected individuals.
    """
    if nd == 'standard':
        pareto_fronts = tools.sortNondominated(individuals, k)
    elif nd == 'log':
        pareto_fronts = tools.sortLogNondominated(individuals, k)
    else:
        raise Exception('divSelect: The choice of non-dominated sorting '
                        'method "{0}" is invalid.'.format(nd))
    # end if
    if DEBUG and logger:
        logger.debug("Num fronts: %d" % len(pareto_fronts))
    # end if

    # Placeholder for Preferability operator
    if int_store["preferability_operator"]["enable"]:
        # 1. Get the objective values
        criteria = _getRawObjValues(individuals)

        # 2. If this is iteration 1, then set the starting
        # preferability thresholds
        if int_store["curr_iter"] == 1:
            _setPreferabilityThresholds(criteria)
        # end if

        # 3. Every *interval* iteration update preferability
        # threshold by a set *percentage*
        interval = int_store["preferability_operator"]["interval"]
        if (int_store["curr_iter"] % interval) == 0:
            _updatePreferabilityThresholds(criteria)
        # end if

        # 4. For each objective that is worse than its
        # current preferability threshold
        # increase solution's rank by 1...
        pareto_fronts = _updateParetoRanks(pareto_fronts)
    # end if

    # Chemical space Diversity with Butina Clustering
    if int_store["diversity_type"] == "chemical":
        sorted_clusters = _genotypeDiversity(individuals)
    # Objective space Diversity with Butina Clustering
    elif int_store["diversity_type"] == "property":
        sorted_clusters = _phenotypeDiversity(individuals)
    else:
        msg = ("Unsupported diversity type: %s"
               % (int_store["diversity_type"]))
        logger.error(msg)
        errors.append((1, msg))
        sorted_clusters = list()
    # end if
    if DEBUG and logger:
        # Print number of clusters
        logger.debug("Num clusters: %d" % len(sorted_clusters))
    # end if

    # Select diverse solutions
    div_solutions = list()
    rank = 0
    while len(div_solutions) < k and rank < len(pareto_fronts):
        # traverse all clusters and preferably select one from each cluster
        for c_idx, cluster in enumerate(sorted_clusters):
            for indi_idx in cluster[1]:
                # if the current individual has the appropriate rank and
                # has not been selected before, then select it
                # and go to next cluster
                indi = individuals[indi_idx]
                if(indi in pareto_fronts[rank] and
                   indi_idx not in div_solutions):
                    div_solutions.append(indi_idx)
                    break
                # end if
            # end for
            if DEBUG and logger:
                # Print current cluster, pareto rank
                # and number of diverse solutions
                logger.debug("Cluster: %d\nRank: %d\nDiv solutions: %d"
                             % (c_idx, rank, len(div_solutions)))
            # end if
        # end for
        # Increase rank by 1
        rank += 1
        # Reset rank if no k solutions have been selected
        if len(div_solutions) < k and rank == len(pareto_fronts):
            rank = 0
        # end if
    # end while
    # Get only the k first diverse solutions if more than k have been selected
    if logger:
        logger.info("Diverse solutions: %d" % len(div_solutions))
    # end if
    div_solutions = div_solutions[:k]
    if logger:
        logger.info("Top %d Diverse solutions: %d" % (k, len(div_solutions)))
    # end if

    chosen = [individuals[idx] for idx in div_solutions]

    return chosen


def _genotypeDiversity(population):
    """Compound diversity based on compounds structure (genotype).

    :param population: A list of individuals to cluster.
    :returns: A list of clusters sorted from largest to smallest.
    """
    from rdkit.Chem import AllChem
    from rdkit import DataStructs
    from rdkit.ML.Cluster import Butina

    # generate fingerprints
    fps = [AllChem.GetMorganFingerprintAsBitVect(indi.result[1], 2, 1024)
           for indi in population]

    # generate the distance matrix:
    dists = []
    nfps = len(fps)
    for i in range(1, nfps):
        sims = DataStructs.BulkTanimotoSimilarity(fps[i], fps[:i])
        dists.extend([1 - x for x in sims])
    # end for
    # now cluster the data:
    clusters = Butina.ClusterData(dists, nfps,
                                  distThresh=0.2,
                                  isDistData=True)

    # sort clusters
    sorted_clusters = list()
    for cluster in clusters:
        sorted_clusters.append((len(cluster), cluster))
    # end for
    # sort from largest to smallest
    sorted_clusters.sort(reverse=True)

    return sorted_clusters


def _phenotypeDiversity(population):
    """Compound diversity based on objective fitness values (phenotype).

    :param population: A list of individuals to cluster.
    :returns: A list of clusters sorted from largest to smallest.
    """
    from rdkit.ML.Cluster import Butina
    from rdkit.DataManip import Metric

    fitness_scores = [indi.fitness.values for indi in population]
    num_comp = len(fitness_scores)
    # euclidean distance matrix
    dists = Metric.GetEuclideanDistMat(fitness_scores)
    # now cluster the data
    clusters = Butina.ClusterData(dists, num_comp,
                                  distThresh=1.0,
                                  isDistData=True)
    # sort clusters
    sorted_clusters = list()
    for cluster in clusters:
        sorted_clusters.append((len(cluster), cluster))
    # end for
    # sort from largest to smallest
    sorted_clusters.sort(reverse=True)

    return sorted_clusters


def _getRawObjValues(individuals):
    """Get the raw values for each objective criteria.

    :param individuals: List of individuals
    :returns: A dictionary with the raw values per objective
              (key: objective name, value: worst objective value).
    """
    # Setup logger
    logger = logging.getLogger("DEAP.GP._getRawObjValues")
    # 1. Get the worst objective values
    # 1.1. Get objective properties names
    criteria = dict()
    for obj in int_store["objectives"]:
        obj_name = obj["objective_property"]
        # Skip objective that does not have an objective_property field
        if "objective_property" not in obj:
            continue
        # end if
        criteria[obj_name] = None
    # end if
    # 1.2. Get the objective values for each objective
    for criteria_name in criteria:
        tmp = list()
        for indi in individuals:
            mol = indi.result[1]
            raw_value = float(mol.GetProp(criteria_name))
            # Replace nan with inf
            if np.isnan(raw_value):
                # raw_value = float('inf')
                raw_value = 0.0
            # end if
            tmp.append(raw_value)
        # end for
        criteria[criteria_name] = np.array(tmp)
        # check the number of the criteria values per objective
        assert(len(criteria[criteria_name]) == len(individuals))
        # 1.1.2.1. Clean the list from 'inf' values...
        num_finite = len([x for x in np.isfinite(criteria[criteria_name])
                            if x])
        if DEBUG:
            logger.debug("Num finite: %d %d" % (num_finite, len(criteria[criteria_name])))
        # end if
        if num_finite:
            criteria[criteria_name] = \
              criteria[criteria_name][np.isfinite(criteria[criteria_name])]
        # end if
    # end for

    return criteria


def _setPreferabilityThresholds(criteria):
    """Set the preferability threshold values for each objective based on the
    worst recorded value for each obective.

    :param criteria: Dictionary of worst value per obective.
    """
    # Setup logger
    logger = logging.getLogger("DEAP.GP._setPreferabilityThresholds")
    # Set the preferability threshold depending on the
    # type of optimization for each objective
    for obj in int_store["objectives"]:
        obj_name = obj["objective_property"]
        # Skip objective that does not have an objective_property field
        if "objective_property" not in obj:
            continue
        # end if
        # When is a minimization then we need the maximum value
        # as a preferability threshold
        if(obj["criteria"]["functor"] == "lessthan" or
           obj["criteria"]["functor"] == "smallaspossible"):
            threshold = max(criteria[obj_name])
        # When is a maximization then we need the minimum value
        # as a preferability threshold
        elif(obj["criteria"]["functor"] == "morethan" or
             obj["criteria"]["functor"] == "largeaspossible"):
            threshold = min(criteria[obj_name])
        # When is a range problem then we need both minimum and maximum
        # values as thresholds
        elif obj["criteria"]["functor"] == "between":
            threshold_min = min(criteria[obj_name])
            threshold_max = max(criteria[obj_name])
            threshold = [threshold_min, threshold_max]
        # end if
        obj["preferability_threshold"] = threshold

        if DEBUG:
            # Print objective preferability threshold
            logger.debug("Objective: %s Preferability Threshold: %s"
                         % (obj_name, obj["preferability_threshold"]))
        # end if
    # end for

    return


def _updatePreferabilityThresholds(criteria):
    """Update preferability thresholds based on the type of optimisation and
    the worst objective value.

    :param criteria: Dictionary of worst value per objective.
    """
    # Setup logger
    logger = logging.getLogger("DEAP.GP._setPreferabilityThresholds")
    #
    percentage = int_store["preferability_operator"]["percentage"]
    for obj in int_store["objectives"]:
        obj_name = obj["objective_property"]
        # Skip objective that does not have an objective_property field
        if "objective_property" not in obj:
            continue
        # end if
        # When is a minimization then we update the preferability
        # threshold by ...
        if(obj["criteria"]["functor"] == "lessthan" or
           obj["criteria"]["functor"] == "smallaspossible"):
            # if the threshold is a positive number then ...
            if obj["preferability_threshold"] >= 0:
                threshold = obj["preferability_threshold"] \
                            * (100 - percentage) / 100
            else:
                threshold = obj["preferability_threshold"] \
                            * (100 + percentage) / 100
            # end if
            if np.isinf(threshold):
                threshold = max(criteria[obj_name])
            # end if
        # When is a maximization then we update the
        # preferability threshold by ...
        elif(obj["criteria"]["functor"] == "morethan" or
             obj["criteria"]["functor"] == "largeaspossible"):
            # if the threshold is a positive number then ...
            if obj["preferability_threshold"] >= 0:
                threshold = obj["preferability_threshold"] \
                            * (100 + percentage) / 100
            else:
                threshold = obj["preferability_threshold"] \
                            * (100 - percentage) / 100
            # end if
            if np.isinf(threshold):
                threshold = min(criteria[obj_name])
            # end if
        # When is a range problem then we update the
        # preferability threshold by ...
        elif obj["criteria"]["functor"] == "between":
            threshold_min = obj["preferability_threshold"][0]
            # if the threshold_min is a positive number then ...
            if threshold_min >= 0:
                threshold_min *= (100 + percentage) / 100
            else:
                threshold_min *= (100 - percentage) / 100
            # end if
            if np.isinf(threshold_min):
                threshold_min = min(criteria[obj_name])
            # end if
            threshold_max = obj["preferability_threshold"][1]
            # if the threshold_max is a positive number then ...
            if threshold_max >= 0:
                threshold_max *= (100 - percentage) / 100
            else:
                threshold_max *= (100 + percentage) / 100
            # end if
            if np.isinf(threshold_max):
                threshold_max = max(criteria[obj_name])
            # end if
            threshold = [threshold_min, threshold_max]
        # end if
        if DEBUG:
            # Print updated preferability threshold
            logger.debug("Objective: %s "
                         "Preferability Threshold: %s "
                         "Threshold: %s"
                         % (obj_name,
                            obj["preferability_threshold"],
                            threshold))
        # end if

        obj["preferability_threshold"] = threshold
    # end for

    return


def _updateParetoRanks(pareto_fronts):
    """Update pareto ranks based on preferability thresholds.
    For each objective that is worse than its current preferability threshold
    increase solution's rank by 1...

    :param pareto_fronts: Current list pareto fronts.
    :returns: Updated list of pareto ranks.
    """
    # Setup logger
    logger = logging.getLogger("DEAP.GP._updateParetoRanks")
    #
    pref_ranks = dict()
    for rank in range(0, len(pareto_fronts)):
        for indi in pareto_fronts[rank]:
            indi_rank = rank
            for obj in int_store["objectives"]:
                obj_name = obj["objective_property"]
                mol = indi.result[1]
                raw_value = float(mol.GetProp(obj_name))
                # Replace nan with inf
                if np.isnan(raw_value):
                    raw_value = float('inf')
                # end if
                # Range
                if obj["criteria"]["functor"] == "between":
                    if obj["preferability_threshold"][0] > raw_value \
                            > obj["preferability_threshold"][1]:
                        indi_rank += 1
                    # end if
                # Maximise
                elif(obj["criteria"]["functor"] == "morethan" or
                     obj["criteria"]["functor"] == "largeaspossible"):
                    if raw_value < obj["preferability_threshold"]:
                        indi_rank += 1
                    # end if
                # Minimise
                elif(obj["criteria"]["functor"] == "lessthan" or
                     obj["criteria"]["functor"] == "smallaspossible"):
                    if raw_value > obj["preferability_threshold"]:
                        indi_rank += 1
                    # end if
                # end if
            # end for
            if DEBUG:
                # Print old and new pareto rank
                logger.debug("Old rank: %d New rank: %d" % (rank, indi_rank))
            # end if
            # Add a list for the new rank...
            if indi_rank not in pref_ranks:
                pref_ranks[indi_rank] = list()
            # end if
            pref_ranks[indi_rank].append(indi)
        # end for
    # end for
    # Update pareto_fronts with the new pareto ranking
    # In Python 3 dictionary's values method returns a dict_values iterator,
    # ans as such a conversion to list is required.
    new_pareto_fronts = list(pref_ranks.values())
    if DEBUG:
        # Print the number of fronts and the size of pareto front
        logger.debug("Num fronts: %d %d"
                     % (len(pareto_fronts), len(new_pareto_fronts)))
        logger.debug("Pareto Front Size: %d %d"
                     % (len(pareto_fronts[0]), len(new_pareto_fronts[0])))
    # end if

    return new_pareto_fronts


def dfsPaths(individual, start, goal, path=None):
    """Return the path from the root to the goal node, in a primitive tree.

    :param individual: A primitive tree.
    :param start: Starting node.
    :param goal: Goal node.
    :param path: The path from start to goal node.
    :returns: Returns the path from start to goal node.
    """
    # Setup logger
    logger = logging.getLogger("DEAP.GP.dfsPaths")
    # If goal is larger than the total length of the individual then error...
    if goal >= len(individual):
        raise IndexError("Invalid goal index (try to reach index %d"
                         " in a tree of size %d)."
                         % (goal, len(individual)))
    # end if
    if DEBUG:
        logger.debug("Start Node: %s\nGoal Node: %s\n Path: %s" % (start, goal, path))
    # end if
    # Get the start node index
    if path is None:
        path = [start]
        dfsPaths.visited = [start]
        if DEBUG:
            logger.debug("Start Node: %s" % start)
        # end if
    # end if
    if DEBUG:
        logger.debug("Start Node: %s"
                     "Goal Node: %s"
                     "Path: %s"
                     "Nodes Visited: %s"
                     % (start, goal, path, dfsPaths.visited))
    # end if
    # When the goal node is reached finish!
    if start == goal:
        if DEBUG:
            logger.debug("Goal Node: %s" % goal)
        # end if
        yield path
    # end if
    #
    if individual[start].arity == 0:
        return
    # end if

    # Traverse tree...
    # Next node to check
    end = start + 1
    total = individual[start].arity
    while total > 0:
        total += individual[end].arity - 1
        if end not in dfsPaths.visited:
            dfsPaths.visited += [end]
            # Recursive DFS, starting from next node
            if DEBUG:
                logger.debug("Current Node: %s"
                             "Goal Node: %s"
                             "Path: %s"
                             % (end, goal, path))
            for p in dfsPaths(individual, end, goal, path + [end]):
                if DEBUG:
                    logger.debug("Current Path: %s" % p)
                # end if
                yield p
            # end for
        # end if
        end += 1
    # end while

    return
###############################################################################


###############################################################################
#                              Intelligent Mutations                           #
###############################################################################
# Inteligent Node Replacement
def mutIntelNodeReplacement(individual, pset):
    """Replaces a randomly chosen primitive from *individual* by a randomly
    chosen *matching* primitive with the same number of arguments
    from the :attr:`pset` attribute of the individual.

    :param individual: The normal or typed tree to be mutated.
    :returns: A tuple of one tree.
    """
    if DEBUG:
        print("%s" % individual)
    # end if

    if len(individual) < 2:
        return individual,
    # end if

    # Do not mutate root...
    index = random.randrange(1, len(individual))
    node = individual[index]

    if node.arity == 0:  # Terminal
        # Get Parent primitive
        index_paths = sorted(list(dfsPaths(individual, 0, index)))
        if DEBUG:
            print("%s" % index_paths)
        # end if
        # Get parent node index (second from last in the first path)
        p_node_index = index_paths[0][-2]

        #
        p_node = individual[p_node_index]
        # inform
        if DEBUG:
            print("Prev Node Index: %d\n"
                  "Prev Node Name: %s\n"
                  "Prev Node Arity: %d\n"
                  "Node Index: %d\n"
                  % (p_node_index, p_node.name, p_node.arity, index))
        # end if
        # get transformation
        prim_name = p_node.name
        trans_idx = reactionsDict[prim_name][0]
        trans = transforms.reactions[trans_idx]

        # find matching compounds
        if index <= p_node_index + p_node.arity:
            arg_idx = index - p_node_index - 1
        else:
            arg_idx = 0
            d = index - p_node_index - 1
            subT_len = 0
            begin = p_node_index + 1
            while d > subT_len:
                slice_ = individual.searchSubtree(begin)
                subT_len += len(individual[slice_])
                begin += len(individual[slice_])
                arg_idx += 1
            # end while
            # # Substract 1 because it adds 1 more at the end of while loop...
            # if arg_idx: arg_idx -= 1
        # end if
        if DEBUG:
            print("Prev Node Index: %d\n"
                  "Prev Node Name: %s\n"
                  "Prev Node Arity: %d\n"
                  "Node Index: %d\n"
                  % (p_node_index, p_node.name, p_node.arity, index))
        # end if
        smarts = Chem.MolToSmarts(trans.rxn.GetReactantTemplate(arg_idx))
        matched_mols = subsMatch[smarts]
        if DEBUG:
            print("Primitive Name: %s\n"
                  "Transform Index: %d\n"
                  "Reactant Index %d\n"
                  "Reactant SMARTS: %s\n"
                  "Matched Mols: %s\n"
                  % (prim_name, trans_idx, arg_idx, smarts, matched_mols))
        # end if
        terms_list = list()
        for smi in matched_mols:
            for term in pset.terminals[node.ret]:
                if repr(smi) == term.name:
                    terms_list.append(term)
                #
            #
        #
        # Empty matched terms list then return individual
        if len(terms_list) == 0:
            return individual,
        # end if
        term = random.choice(terms_list)

        # term = random.choice(pset.terminals[node.ret])
        if isclass(term):
            term = term()
        individual[index] = term
    else:  # Primitive
        # Get subtrees
        subtrees = [None] * node.arity
        # Starting point of first subtree
        begin = index + 1
        for i in range(node.arity):
            # Get subtree
            slice_ = individual.searchSubtree(begin)
            # Convert subtree to a primitive tree
            # in order to be able to compile it
            subtree = gp.PrimitiveTree(individual[slice_])
            st_res = gp.compile(subtree, pset)
            subtrees[i] = (individual[slice_], st_res)
            # Update starting point
            begin += len(subtree)
        #
        if DEBUG:
            print("\n>>> ", subtrees, " <<<")
        # end if
        #
        # get primitives with same number of arguments
        prims = [p for p in pset.primitives[node.ret] if p.args == node.args]

        prims_list = list()
        for p in prims:
            prim_name = p.name
            # get transformation
            trans_idx = reactionsDict[prim_name][0]
            trans = transforms.reactions[trans_idx]
            OK_flag = False
            for arg_idx in range(trans.numReactants):
                # get pattern
                pattern = trans.rxn.GetReactantTemplate(arg_idx)
                # check coresponding subtree if it has a substructure match
                # with the appropriate reactant pattern
                if isinstance(subtrees[arg_idx][1], str):
                    reactant = Chem.MolFromSmiles(subtrees[arg_idx][1])
                elif isinstance(subtrees[arg_idx][1], tuple):
                    reactant = subtrees[arg_idx][1][1]
                # end if
                if DEBUG:
                    print(">>> %s %s <<<" % (Chem.MolToSmarts(pattern),
                                             Chem.MolToSmiles(reactant)))
                # end if
                if reactant:
                    try:
                        reactant = Chem.AddHs(reactant)
                    except:
                        pass
                    # end try-except
                    if reactant.HasSubstructMatch(pattern):
                        OK_flag = True
                    else:
                        OK_flag = False
                    # end if
                else:
                    OK_flag = False
                # end if
                if DEBUG:
                    print(">>> ", prim_name, OK_flag, arg_idx, " <<<")
                # end if
            # end for
            # Add primitive to the list of matching primitives
            # only if *OK_flag* is *True*,
            # that means all reactant patterns have a match
            if OK_flag:
                if DEBUG:
                    print(">>> ", prim_name, OK_flag, " <<<")
                # end if
                prims_list.append(p)
            # end if
        # end for
        if DEBUG:
            print(">>> ", prims_list, "<<<\n")
        # end if
        # If not found any matching primitive then return individual
        if len(prims_list) == 0:
            return individual,
        # end if

        individual[index] = random.choice(prims_list)

    return individual,


# Intelligent Insert
def mutIntelInsert(individual, pset):
    """Inserts a new branch at a random position in *individual*. The subtree
    at the chosen position is used as child node of the created subtree, in
    that way, it is really an insertion rather than a replacement. Note that
    the original subtree will become one of the children of the new primitive
    inserted, but not perforce the first (its position is randomly selected if
    the new primitive has more than one child).

    :param individual: The normal or typed tree to be mutated.
    :returns: A tuple of one tree.
    """
    index = random.randrange(len(individual))
    node = individual[index]
    slice_ = individual.searchSubtree(index)
    choice = random.choice

    # Convert subtree to a primitive tree in order to be able to compile it
    subtree = gp.PrimitiveTree(individual[slice_])
    # subtree SMILES
    st_res = gp.compile(subtree, pset)
    if DEBUG:
        print("HERE >>> ", st_res)
    # end if
    # subtree molecule
    if isinstance(st_res, str):
        #reactant = mols[repr(st_res)]
        reactant = mols[st_res]
    elif isinstance(st_res, tuple):
        reactant = st_res[1]
    # end if
    if DEBUG:
        print("HERE >>> ", reactant, reactant.GetProp("_Name"))
    # end if

    primitives = list()
    for p in pset.primitives[node.ret]:
        # As we want to keep the current node as children of the new one,
        # it must accept the return value of the current node
        if node.ret in p.args:
            prim_name = p.name
            # get transformation
            trans_idx = reactionsDict[prim_name][0]
            trans = transforms.reactions[trans_idx]
            OK_flag = False
            # Keep primitives that can use the selected subtree as reactant
            # and also the index of the matching reactant
            for idx in range(trans.numReactants):
                # get pattern
                pattern = trans.rxn.GetReactantTemplate(idx)
                # check coresponding subtree if it has a substructure match
                # with the appropriate reactant pattern
                if DEBUG:
                    print(">>> ", \
                          Chem.MolToSmarts(pattern), \
                          Chem.MolToSmiles(reactant), " <<<")
                # end if
                if reactant:
                    try:
                        reactant = Chem.AddHs(reactant)
                    except:
                        pass
                    # end try-except
                    if reactant.HasSubstructMatch(pattern):
                        OK_flag = True
                        primitives.append((p, idx))
                    else:
                        OK_flag = False
                    # end if
                else:
                    OK_flag = False
                # end if
                if DEBUG:
                    print(">>> ", prim_name, OK_flag, idx, " <<<")
                # end if
            # end for
        # end if
    # end for

    if len(primitives) == 0:
        return individual,

    # Get a random choice of matching primitive
    # (transformation) and the reactant matching position
    new_node, position = choice(primitives)
    new_subtree = [None] * len(new_node.args)

    #
    prim_name = new_node.name
    # get transformation
    trans_idx = reactionsDict[prim_name][0]
    trans = transforms.reactions[trans_idx]

    for i, arg_type in enumerate(new_node.args):
        # select a terminal to attach as an argument
        if i != position:
            # get pattern
            smarts = Chem.MolToSmarts(trans.rxn.GetReactantTemplate(i))
            # find matching ingredients
            matched_mols = subsMatch[smarts]
            terms_list = list()
            for smi in matched_mols:
                for term in pset.terminals[node.ret]:
                    if repr(smi) == term.name:
                        terms_list.append(term)
                    # end if
                # end for
            # end for
            # Empty matched terms list then return individual
            if len(terms_list) == 0:
                return individual,
            # end if
            term = choice(terms_list)
            #
            if isclass(term):
                term = term()
            new_subtree[i] = term

    new_subtree[position:position + 1] = individual[slice_]
    new_subtree.insert(0, new_node)
    individual[slice_] = new_subtree

    return individual,
###############################################################################


# TODO: Move stuff from ReactMOGP as DEAP need all to be in a single file???
def runGP(starting_molecules, transformations, internal_storage, debug=False):
    """The main function for running the Genetic Programming algorithm.

    :param starting_molecules:
    :param transformations:
    :param internal_storage:
    :param debug:
    :returns:
    """
    # start timer
    start_time = datetime.now()

    # Set debug mode
    global DEBUG
    DEBUG = debug

    # Get logger
    logger = logging.getLogger('DEAPGenProgMod.runGP')

    # 1. Create a molecules dictionary for matching
    # starting molecules to terminals
    global mols
    mols = dict((Chem.MolToSmiles(m, isomericSmiles=True), m)
                for i, m in enumerate(starting_molecules))

    # 2. Assign self.reactions to DEAPGenProgMod.transforms
    global transforms
    transforms = transformations

    # 3. Update internal storage
    global int_store
    int_store = internal_storage

    # 4. Initialize multi-\many-objective scorers
    initScorers(logger)

    # 5. Find matching starting ingredients for the transformations
    global subsMatch
    subsMatch = subStructureMatching(logger)

    # 6. Initialize Primitive Set
    pset = initPrimitiveSet()

    # 7. Initialize fitness weights for the GP
    fit_weights = initFitnessWeights()

    # Debug info
    if DEBUG:
        logger.debug("Initialisation completed.")
    # end if

    # 8. Construct the GP algorithm
    # 8.1 Set Multi-\Many-Objective Fitness
    creator.create("FitnessMulti", base.Fitness, weights=fit_weights)
    # 8.2 Set Individual Chromosome
    creator.create("Individual",
                   gp.PrimitiveTree,
                   fitness=creator.FitnessMulti,
                   pset=pset,
                   result=None)
    # Debug info
    if DEBUG:
        logger.debug("Setup DEAP GP Individual.")
    # end if

    # 8.3 Initialize GP toolbox
    toolbox = base.Toolbox()
    # # 8.4  Map functions for GP process using multiprocessing Map function
    # executor = futures.ProcessPoolExecutor(CPU_COUNT)
    # toolbox.register("map", executor.map)
    # 8.5 Structure initializer
    # 8.5.1 Individual init function
    toolbox.register("expr_init", gp.genFull, pset=pset, min_=1, max_=1)
    # 8.5.2 Population init function
    toolbox.register("individual", tools.initIterate, creator.Individual,
                     toolbox.expr_init)
    toolbox.register("population", initPopulation, list,
                     toolbox.individual,
                     pset=pset, logger=logger)

    # Debug info
    if DEBUG:
        logger.debug("Setup DEAP GP Population.")
    # end if

    # 8.6 Evaluation & Selection
    # 8.6.1 Evaluation
    toolbox.register("evaluate", evalIndividual)
    # Debug info
    if DEBUG:
        logger.debug("Setup DEAP GP Individual Evaluation.")
    # end if

    # 8.7 Selection
    # toolbox.register("select", tools.selNSGA2)
    # toolbox.register("select", tools.selSPEA2)
    toolbox.register("select", divSelect)
    # Debug info
    if DEBUG:
        logger.debug("Setup DEAP GP Diversity Selection.")
    # end if

    # 8.8 Mutations & Crossover
    # 8.8.1 Crossover
    toolbox.register("mate", gp.cxOnePoint)
    # 8.8.2 Mutations (Built-in)
    toolbox.register("expr_mut", gp.genGrow, min_=1, max_=1)
    toolbox.register("mutateGrow", gp.mutUniform, expr=toolbox.expr_mut,
                     pset=pset)
    toolbox.register("mutateShrink", gp.mutShrink)
    # 8.8.3 Intelligent Mutations
    toolbox.register("mutateIntelNodeReplace",
                     mutIntelNodeReplacement,
                     pset=pset)
    toolbox.register("mutateIntelInsert",
                     mutIntelInsert,
                     pset=pset)
    # 8.8.4 Mutations list
    mutations_list = [toolbox.mutateGrow,
                      toolbox.mutateShrink,
                      toolbox.mutateIntelNodeReplace,
                      toolbox.mutateIntelInsert, ]
    toolbox.register("mutate", mutations_list[0], pset=pset)
    # Debug info
    if DEBUG:
        logger.debug("Setup DEAP GP Evolutionary Operators.")
    # end if

    # 9 History
    history = tools.History()
    # 9.1 Decorate the variation operators
    toolbox.decorate("mate", history.decorator)
    toolbox.decorate("mutate", history.decorator)
    # Debug info
    if DEBUG:
        logger.debug("Setup DEAP GP History decorators.")
    # end if

    # 10. Static Limit to Tree height
    tree_height_limit = gp.staticLimit(key=operator.attrgetter("height"),
                                       max_value=6)
    # 10.1 Decorate with treeHeightLimit
    toolbox.decorate("mate", tree_height_limit)
    toolbox.decorate("mutate", tree_height_limit)
    # Debug info
    if DEBUG:
        logger.debug("Setup DEAP GP Individual Height Limit decorator.")
    # end if

    # 11 Setup stats
    # 11.1. Pareto Front
    pf = tools.ParetoFront(similar=findSame)
    # 11.2. Statistics
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    # Debug info
    if DEBUG:
        logger.debug("Setup DEAP GP Pareto Front and Statistics.")
    # end if

    # 12 Initialize GP search parameters
    # 12.1 Crossover Rate
    CXPB = 0
    # 7.11.2 Mutation Rate
    MUTPB = 1.0
    # 7.11.3 Maximum number of generations
    NGEN = internal_storage["max_iter"]
    # 7.11.4 Population size
    POP_SIZE = internal_storage["population_size"]
    # Debug info
    if DEBUG:
        logger.debug("Setup DEAP GP Search Parameters.")
        logger.debug("Crossover Rate: %d "
                     "Mutation Rate: %d "
                     "Generations: %d "
                     "Population Size: %d"
                     % (CXPB, MUTPB, NGEN, POP_SIZE))
    # end if

    # 13. Generate initial population
    task_start = datetime.now()
    # Debug info
    if DEBUG:
        logger.debug("Starting DEAP GP Population generation.")
    # end if
    pop = toolbox.population(n=POP_SIZE)
    task_elapsed = datetime.now() - task_start
    logger.info("Initial Population - Time elapsed: %s\n"
                % task_elapsed)
    # 13.1 Update History
    history.update(pop)
    # Debug info
    if DEBUG:
        logger.debug("Generated DEAP GP Initial Population.")
    # end if

    # 14. Logbook
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + stats.fields

    # 15 Get individuals with an invalid fitness (all in this stage)
    invalid_ind = [ind for idx, ind in enumerate(pop) if not ind.fitness.valid]

    # 15.4 Score individuals
    multiScoring(invalid_ind, logger)

    # 15.2 Evaluate individuals
    fitness = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitness):
        ind.fitness.values = fit
    # end for
    assert(len(pop) == len(invalid_ind))

    # 16 Update archives
    updateArchive(pop, logger)
    updateSolutionsArchive(pop, logger)

    # 17 Update pareto front
    pf.update(pop)
    non_dom = pf.items
    logger.info("Pareto Front Size: %d" % len(non_dom))

    # 18 Update elite archive
    updateEliteArchive(non_dom, logger)

    # 19 Calculate statistics
    record = stats.compile(pop)
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    print("%s" % logbook.stream)

    # Progress Bar!!!
    widgets = ["GP Algorithm Generations: ",
               progressbar.Percentage(), " ",
               progressbar.ETA(), " ",
               progressbar.Bar()]
    pbar = progressbar.ProgressBar(widgets=widgets, max_val=NGEN)
    # Progress bar start
    pbar.start()

    # TODO: GP LOOP!!!
    # For each generation
    for gen in range(1, NGEN + 1):
        # Get current iteration number
        int_store["curr_iter"] = gen
        # For each mutation
        # Gather the new offsprings
        invalid_offsprings = list()
        n_evals = 0
        for mutation in mutations_list:
            # Decorate the current mutation as mutate
            if mutation.__name__ == "mutateShrink":
                toolbox.register("mutate", mutation)
            else:
                toolbox.register("mutate", mutation, pset=pset)
            # end if

            # Decorate the variation operators
            toolbox.decorate("mate", history.decorator)
            toolbox.decorate("mutate", history.decorator)

            # Tree height limit decoration
            toolbox.decorate("mate", tree_height_limit)
            toolbox.decorate("mutate", tree_height_limit)
            offsprings = algorithms.varAnd(pop, toolbox, CXPB, MUTPB)

            # Get valid mutated offsprings
            invalid_ind = [ind for ind in offsprings if not ind.fitness.valid]
            res = toolbox.map(gp.compile,
                              invalid_ind, (pset for _ in invalid_ind))
            res = toolbox.map(_zip, invalid_ind, res)
            # Keep only the ones that have a valid output
            invalid_ind = list()
            for ind, r in res:
                ind.result = r
                n_evals += 1
                if r:
                    invalid_ind.append(ind)
                # end if
            # end for
            if DEBUG:
                logger.debug(">>> %d <<<" % len(invalid_ind))
            # end if

            # Add mutated offsprings
            invalid_offsprings += invalid_ind
            if DEBUG:
                logger.debug(">>> %d <<<" % len(invalid_offsprings))
            # end if
        # end for

        # Calculate fitness scores for all valid offsprings
        multiScoring(invalid_offsprings, logger)

        # Evaluate the individuals with an invalid fitness
        fitnesses = toolbox.map(toolbox.evaluate, invalid_offsprings)
        for ind, fit in zip(invalid_offsprings, fitnesses):
            ind.fitness.values = fit
        # end for

        # Update archives
        updateArchive(pop, logger)
        updateSolutionsArchive(pop, logger)

        # Add individuals from elite archive to population
        pop = getEliteArchive(pop, logger)

        # Population Selection
        logger.info("Before Selection: %d" % len(pop))
        pop = toolbox.select(pop, POP_SIZE)
        logger.info("After Selection: %d" % len(pop))

        # Update pareto front
        pf.update(pop)
        non_dom = pf.items
        logger.info("Pareto Front: %d" % len(non_dom))

        # Update elite archive
        updateEliteArchive(non_dom,logger)

        # Parallel Coordinates Figure
        # plotParallelCoords(pop, iteration=gen, logger=logger)

        # Append the current generation statistics to the logbook
        # statistics for current population
        record = stats.compile(pop)
        logbook.record(gen=gen, nevals=n_evals, **record)
        print(logbook.stream)

        # Update progress bar
        pbar.update(gen)
    # end for
    # Progress bar finish
    pbar.finish()

    # TODO: Get final solutions
    # final_solutions = getFinalSolutions(pop)
    # Plot Solutions Trees
    # plotSolutions(pop, logger)
    # Plot History Genealogy
    # plotGenealogy(history, toolbox, logger)
    # Create Archives
    # archives = _saveArchivestoFiles()

    # stop timer & calculate elapsed time
    elapsed_time = datetime.now() - start_time
    logger.info("Time elapsed: %s\n" % elapsed_time)

    # TODO: Gather results
    results = dict()
    results["status"] = False
    results["message"] = ""
    if len(errors):
        results["status"] = True
        results["message"] = "\n".join(e[-1] for e in errors)
    # end if
    results["work_dir"] = int_store["logs_dir"]
    # results["final_solutions_smiles"] = final_solutions[0]
    # results["final_solutions_image"] = final_solutions[1]
    # results["final_solutions_excel"] = final_solutions[2]
    # results["final_solutions_html"] = final_solutions[3]
    # results["archives"] = archives

    # check if status is True then there is an error
    if results["status"]:
        logger.warning("Errors:\n%s\n" % (results["message"]))
    # end if

    return results
