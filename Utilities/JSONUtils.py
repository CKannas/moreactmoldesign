"""
Utility functions for JSON related actions.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"


def convert(input):
    """Convert JSON loaded Python object to Python object.

    :param input: JSON loaded Python object.
    :returns: Python object."""
    # dictionary
    if isinstance(input, dict):
        return dict((convert(key), convert(value))
                    for key, value in input.items())
    # list
    elif isinstance(input, list):
        return list(convert(element) for element in input)
    # unicode
    elif isinstance(input, str):
        # try to evaluate a string to int or float
        try:
            return int(input)
        except ValueError:
            pass
        try:
            return float(input)
        except ValueError:
            # return string
            return input
    # other -> string
    else:
        return input
    # end if
