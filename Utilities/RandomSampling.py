"""
Utility functions for random sampling from various objects.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"

import random
import time

from MOReactMolDesign.Utilities import PickleUtils

# Random seed
random.seed(time.time())


def iterSample(iterable, sample_size):
    """Random sampling from iterable object.

    :param iterable: Iterable object.
    :param sample_size: Sample size.
    :return: List of randomly selected objects."""
    results = list()

    for i, v in enumerate(iterable):
        # random integer from 0 to i
        r = random.randint(0, i)
        if r < sample_size:
            if i < sample_size:
                # add first samplesize items in random order
                results.insert(r, v)
            else:
                # at a decreasing rate, replace random items
                results[r] = v
            # end if
        # end if
    # end for

    return results


def pklFileSample(file_name, sample_size):
    """Random sampling from contents of pickle file.

    :param file_name: File name of the pickle file.
    :param sample_size: Sample size.
    :return: List of randomly selected objects."""
    results = list()

    with open(file_name, 'rb') as f:
        i = 0
        while True:
            try:
                v = PickleUtils.unpickleFileToObj(f)
                # random integer from 0 to i
                r = random.randint(0, i)
                if r < sample_size:
                    if i < sample_size:
                        # add first samplesize items in random order
                        results.insert(r, v)
                    else:
                        # at a decreasing rate, replace random items
                        results[r] = v
                    # end if
                # end if
                i += 1
            except EOFError:
                break
            # end try-except
        # end while
    # end with

    return results
