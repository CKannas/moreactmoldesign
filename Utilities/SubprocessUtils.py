from __future__ import print_function

"""
Subprocess utilities module.

A module for subprocess utilities.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"

import os
import sys
import subprocess
import signal
import tempfile


def runProcessRT(cmd, enableStdout=True, enableStderr=True):
    """Run a process with real time output.

    :param cmd: Command line for execution, list or string.
    :param enableStdout: Boolean, enable/disable output to stdout.
    :param enableStderr: Boolean, enable/disable output to stderr.
    :returns: Status, Stdout and Stderr strings."""
    try:
        # Create temporary files for capturing child process stdout and stderr
        stdout_fd, stdout_tname = tempfile.mkstemp(text=True)
        stderr_fd, stderr_tname = tempfile.mkstemp(text=True)
        # Open files for writing
        stdout_fd = os.fdopen(stdout_fd, "a+")
        stderr_fd = os.fdopen(stderr_fd, "a+")

        # child process
        if sys.platform.startswith("linux"):
            process = subprocess.Popen(cmd,
                                       stdout=stdout_fd,
                                       stderr=stderr_fd,
                                       preexec_fn=os.setsid,
                                       universal_newlines=True)
        elif sys.platform.startswith("win"):
            process = subprocess.Popen(cmd,
                                       stdout=stdout_fd,
                                       stderr=stderr_fd,
                                       universal_newlines=True)
        # end if
        # Wait child process to terminate
        process.wait()
    except KeyboardInterrupt:
        os.killpg(process.pid, signal.SIGINT)
    except:
        print("Unexpected error: {0}".format(sys.exc_info()[0]))
        raise
    finally:
        # close temp files
        stdout_fd.close()
        stderr_fd.close()
        stdout = stderr = ""
        with open(stdout_tname, "r") as f:
            stdout = "".join(f.readlines())
        # end with
        with open(stderr_tname, "r") as f:
            stderr = "".join(f.readlines())
        # end with
        # Print stdout and/or stderr
        if enableStdout:
            sys.stdout.write(stdout)
        # end if
        if enableStderr:
            sys.stderr.write(stderr)
        # end if
        returncode = process.returncode
        if not returncode:
            returncode = 0
        # end if
        # Remove temp files
        os.unlink(stdout_tname)
        os.unlink(stderr_tname)
    # end try-except-finally

    return returncode, stdout, stderr
