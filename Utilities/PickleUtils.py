"""
Utility functions for pickle related actions.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"

import pickle


def pickleObjToStr(obj):
    """Pickle an object to a string.

    :param obj: Python object.
    :returns: String."""
    return pickle.dumps(obj, pickle.HIGHEST_PROTOCOL)


def unpickleStrToObj(pkl):
    """Unpickle a pickled string to an object.

    :param pkl: Python pickled string.
    :returns: Python object."""
    return pickle.loads(pkl)


def pickleObjToFile(obj, file_desc):
    """Pickle an object to a file.

    :param obj: Python object."""
    pickle.dump(obj, file_desc, pickle.HIGHEST_PROTOCOL)

    return


def unpickleFileToObj(file_desc):
    """Unpickle a pickled object from file.

    :param file_desc: File descriptor.
    :returns: Python object."""
    return pickle.load(file_desc)


def unpickleFileToIter(file_name):
    """Unpickle a pickled file to an iterator object.

    :param file_desc: File descriptor.
    :returns: Python iterator."""
    with open(file_name, "rb") as f:
        while True:
            try:
                obj = unpickleFileToObj(f)
                yield obj
            except EOFError:
                break
            # end try-except
        # end while
    # end with

    return
