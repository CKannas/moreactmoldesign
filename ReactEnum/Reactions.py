from __future__ import print_function

"""
Module that provides functionalities for Reaction Enumeration
wrapped around RDKit's Reaction functionalities.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"

# Python imports
import os
import logging
import gc
from datetime import datetime
from collections import namedtuple
import json
import progressbar
import tempfile

# Multiprocessing imports
import threading
import multiprocessing
from concurrent import futures

# RDKit imports
from rdkit import Chem
from rdkit.Chem.PropertyMol import PropertyMol
from rdkit.Chem import AllChem
from rdkit.Chem import FunctionalGroups

# MOReactMolDesign imports
from MOReactMolDesign import Config
from MOReactMolDesign.Utilities import PickleUtils, \
                                       RandomSampling
from MOReactMolDesign.CLI_Tools.Support import TCPServerLogger

# Named tuples required to find functional groups in molecules and reactions
molsFG = namedtuple("MolsFG", ["mols", "pattern"])
rxnFG = namedtuple("ReactionFG", ["reaction", "pattern"])
reactFG = namedtuple("ReactantFG", ["reactionIdx", "reactants"])

logger = logging.getLogger('ReactEnum.Reactions')

initial_timestamp = datetime.now()
CPU_COUNT = multiprocessing.cpu_count()  # Utilise all available cores


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class ReactionContainer(object):
    """Container object class to store information and expose functionality
    related to chemical reactions."""

    def __init__(self, rid, name, smirks):
        """Constructor method.

        :param rid: Reaction Id.
        :param name: Reaction name.
        :param smirks: Reaction's SMIRKS encoding.'"""
        # Reaction Id
        self.reactionId = rid
        # Reaction Name
        self.name = name
        # Reactions' Initial SMIRKS
        self.smirks = smirks
        # Reactions' RDKit object
        self.rxn = AllChem.ReactionFromSmarts(self.smirks)
        # Number of reactants
        self.numReactants = self.rxn.GetNumReactantTemplates()
        # Number of products
        self.numProducts = self.rxn.GetNumProductTemplates()
        # Compute 2D coordinates
        self._compute2DCoordinates()
        # Unique compounds SMILES
        self.unique = set()
        self.num_products = 0
        self.products = iter([])
        # Timestamp
        self.timestamp = initial_timestamp
        # Additional properties
        self.properties = dict()

        return

    def _compute2DCoordinates(self):
        """Compute 2D coordinates for reactants and products."""
        # Reactants
        for i in range(self.numReactants):
            reactant = self.rxn.GetReactantTemplate(i)
            reactant.Compute2DCoords()
        # end for
        # Products
        for i in range(self.numProducts):
            product = self.rxn.GetProductTemplate(i)
            product.Compute2DCoords()
        # end for

        return

    def addProperty(self, name, value):
        """Add additional properties to the reaction.

        :param name: Property name.
        :param value: Property value."""
        try:
            self.properties[name] = eval(value)
        except SyntaxError:
            self.properties[name] = repr(value)
        # end try - except

        return

    def getProperty(self, name, def_val=None):
        """Get the value of requested property if is available.

        :param name: The property name.
        :param def_val: The default value to be returned in case 'name' does not exist.
        :return: The property of the value or None."""

        return self.properties.get(name, def_val)

    def displayInfo(self):
        """Reaction information for display purpose.

        :returns: A string with the transformation's information for display."""
        info = "Reaction Id: %s\n" % self.reactionId
        info += "Reaction Name: %s\n" % self.name
        info += "Reaction SMIRKS: %s\n" % (self.printSmirks())
        # Add any additional properties to the information block...
        for pname, pval in self.properties.items():
            info += "%s: %s\n" % (pname, pval)
        # end for

        return info

    def printSmirks(self):
        """Print SMIRKS/SMARTS encoding of the reaction.

        :returns: Reaction's SMIRKS string."""

        return AllChem.ReactionToSmarts(self.rxn)

    def saveRxnFile(self, fileName=None):
        """Save reaction to an Rxn file.

        :param fileName: The name of the file to save reactions.
        """
        if not fileName:
            fileName = ("reaction_%s.rxn" % self.reactionId)
        with open(fileName, "w") as outFile:
            outFile.write(AllChem.ReactionToRxnBlock(self.rxn))

        return

    def findMatchingReactants(self, react_mols):
        """Return a tuple of tuples with matching reactants for the reaction.

        :param react_mols: List of RDKit molecule objects.
        :returns: A tuple of tuples, one for each reactant pattern,
                  with matching RDKit molecule objects."""
        matched = []

        # For each of reactions reactants find the compounds
        # that have the same substructure
        for i in range(self.numReactants):
            pattern = self.rxn.GetReactantTemplate(i)
            matched.append([])
            for m in react_mols:
                # Add explicit and implicit hydrogens to compounds
                # so that the substructure search matching can be applied
                # on reactions mapped hydrogen atoms
                try:
                    m = Chem.AddHs(m)
                except:
                    # Debug log
                    logger.warning("Cannot add hydrogens to %s"
                                   % (Chem.MolToSmiles(m, isomericSmiles=True)))
                # end try-except
                if m and m.HasSubstructMatch(pattern):
                    # Convert to PropertyMol
                    m = PropertyMol(m)
                    matched[i].append(m)
                    # Debug log
                    logger.debug("%s has substructure match %s"
                                 % (Chem.MolToSmiles(AllChem.RemoveHs(m),
                                                     isomericSmiles=True),
                                    Chem.MolToSmarts(pattern,
                                                     isomericSmiles=True)))
                # end if
            # end for
        # end for

        # inform
        logger.info("Matching Reactants for reaction: %s" % self.reactionId)
        logger.info("Found %d list(s) of matching reactants." % (len(matched)))
        for i, rl in enumerate(matched):
            logger.info("Found %d matching reactants for reaction's "
                        "%d reactant." % (len(rl), i + 1))
        # end for

        # Convert to tuple
        matched = [tuple(r) for r in matched]
        return tuple(matched)

    def IsEnumerate(self, reactants):
        """Check whether the reaction can be enumerated.

        :param reactants: List/Tuple of tuples/lists of molecules.
        :returns: 0 if reaction cannot be enumerated, 1 otherwise."""
        if not (isinstance(reactants, tuple) or isinstance(reactants, list)):
            logger.warning("Parameter 'reactants' is neither "
                           "a list or a tuple.")
            return 0
        elif len(reactants) != self.numReactants:
            logger.warning("The number of provided reactants is not the same "
                           "as the number of reactants required by "
                           "this reaction.")
            return 0
        elif len(reactants) == 0:
            logger.warning("The number of provided reactants is 0.")
            return 0
        elif not (isinstance(reactants[-1], tuple) or
                  isinstance(reactants[-1], list)):
            logger.warning("Parameter 'reactants' is not a list/tuple of "
                           "lists/tuples of reactants.")
            return 0
        elif len(reactants) == self.numReactants:
            for i in range(self.numReactants):
                if len(reactants[i]) == 0:
                    logger.warning("Reaction Id: %s Empty reactants "
                                   "list/tuple." % self.reactionId)
                    return 0
                # end if
            # end for
        # end if

        return 1

    def EnumerateReaction(self, reactants):
        """Enumerate the products of the reaction based on the
        provided reactants.

        :param reactants: List/Tuple of tuples/lists of molecules.
        :returns: List of RDKit molecules objects."""
        # Check if reaction can be enumerated...
        if not self.IsEnumerate(reactants):
            self.products = iter([])

            return

        # Add explicit Hydrogens
        reactants = list(reactants)
        for i in range(len(reactants)):
            # Add explicit hydrogens to reactants, if posible
            tmp = []
            for m in reactants[i]:
                try:
                    tmp.append(Chem.AddHs(m))
                except:
                    tmp.append(m)
                    # Debug log
                    logger.warning("Cannot add hydrogens to %s"
                                   % (Chem.MolToSmiles(m, isomericSmiles=True)))
                    # pass
                # end try-except
            # end for
            reactants[i] = tmp
        # Enumerate products
        self.products = self.myEnumerateLibraryFromReaction(reactants)
        # Reset
        self.unique = None
        # Update timestamp
        self.timestamp = datetime.now()

        return

    def myEnumerateLibraryFromReaction(self, sidechainSets):
        """Rewritten Enumerate Library From Reaction to provide additional
        information for the products.

        :param sidechainSets: List/Tuple of tuples/lists of RDKit molecules.
        :returns: List of RDKit molecules objects."""
        # Check if number of chainset provided are equal
        # to the number of reactants
        if len(sidechainSets) != self.rxn.GetNumReactantTemplates():
            raise Error(ValueError, '%d sidechains provided, %d required'
                        % (len(sidechainSets),
                           self.rxn.getNumReactantTemplates()))
        # end if

        #######################################################################
        def _combiEnumerator(items, depth=0):
            """Enumerate sidechains combinations.

            :param items: List of RDKit molecules objects.
            :param depth: Integer starting from 0.
            :returns: List of RDKit molecules objects."""
            for item in items[depth]:
                if (depth + 1) < len(items):
                    v = _combiEnumerator(items, depth + 1)
                    for entry in v:
                        l = [item]
                        l.extend(entry)
                        yield l
                    # end for
                else:
                    yield [item]
                # end if
            # end for
            return
        #######################################################################
        task_start = datetime.now()
        # Start TCP Server Logger
        tcpserver = TCPServerLogger.LogRecordSocketReceiver()

        host, port = tcpserver.getsockname()
        # debug information
        logger.debug("TCPServerLogger socket: %s, %s" % (host, port))

        # Start a thread with the server -- that thread will then start one
        # more thread for each request
        server_thread = threading.Thread(target=tcpserver.serve_until_stopped)
        # Exit the server thread when the main thread terminates
        server_thread.setDaemon(True)
        server_thread.start()

        # Parallel execution of RunReactants
        with futures.ProcessPoolExecutor(CPU_COUNT - 1) as executor:
            # Submit a set of asynchronous jobs
            jobs = []
            # Create an iterator of up to 1000 random chains for enumeration
            # This is done to lower the execution time of the reaction enumeration
            random_chains = iter(RandomSampling.iterSample(_combiEnumerator(sidechainSets), 1000))
            # Enumerate chains in parallel
            # for chains in _combiEnumerator(sidechainSets):
            for chains in random_chains:
                # Add product(s) reaction history
                productHistory = self._historyTree(chains)
                rxn = self.rxn
                chains = list(chains)
                #
                job = executor.submit(_parallelRunReactants,
                                      host,
                                      port,
                                      rxn,
                                      chains,
                                      productHistory)
                jobs.append(job)
            # end for

            logger.info("Reaction Id: %s - Number of jobs: %s"
                        % (self.reactionId, len(jobs)))

            # Gather results
            # Progress Bar
            widgets = ["Reaction Id: %4s - Enumeration Jobs Completed: "
                       % self.reactionId,
                       progressbar.Percentage(), " ",
                       progressbar.ETA(), " ",
                       progressbar.Bar()]
            pbar = progressbar.ProgressBar(widgets=widgets, max_value=len(jobs))
            # Progress bar start
            pbar.start()
            # results
            total_res = list()
            for job in futures.as_completed(jobs):
                prods_file, products_count = job.result()
                # Open the file for reading only if it has something to read...
                if products_count:
                    products = PickleUtils.unpickleFileToIter(prods_file)
                    products = list(products)
                    yield products
                # end if
                # Delete the temporary file
                os.unlink(prods_file)

                total_res.append((products_count, prods_file))
                # Progress bar update
                pbar.update(len(total_res))
            # end for
            # Progress bar finish
            pbar.finish()
            # assert
            assert(len(jobs) == len(total_res))
        # end with
        # All jobs have finished, send shutdown signal
        # to TCP Server Logger
        tcpserver.abort = 1
        # Wait for TCP Server Logger thread to finish
        server_thread.join(2)
        logger.debug("TCPServerLogger is alive: %s" % (server_thread.isAlive()))

        task_elapsed = datetime.now() - task_start
        logger.info("Reaction Id: %s - Time Elapsed: %s"
                    % (self.reactionId, task_elapsed))

        return

    def _historyTree(self, mols):
        """Create the history tree for the products.

        :param mols: List of RDKit molecules objects.
        :returns: JSON string with the production history tree."""
        histTree = dict()
        histTree["reaction"] = self.reactionId
        histTree["reactants"] = list()
        # Create a history tree from input molecules history (if available)
        for mol in mols:
            tmp = dict()
            tmp["molId"] = mol.GetProp("_Name")
            # If a molecule has history attach to it...
            if mol.HasProp("History"):
                # use it...
                tmp.update(json.loads(mol.GetProp("History")))
            else:
                # else add an 'empty' history node...
                tmp["reaction"] = None
                tmp["reactants"] = list()
            # end if
            histTree["reactants"].append(tmp)
        # end for
        # convert to JSON string and remove spaces
        histTree = json.dumps(histTree).replace(" ", "")

        return histTree

    def _updateHistoryTree(self, mol):
        """Update history tree with molecule's id.

        :param mol: RDKit molecule object.
        :returns: Updated RDKit molecule object."""
        histTree = json.loads(mol.GetProp("History"))
        histTree["molId"] = mol.GetProp("_Name")
        histTree = json.dumps(histTree).replace(" ", "")
        mol.SetProp("History", histTree)

        return mol

    def _setProductName(self, mol):
        """Add a name to the input compound.

        :param mol: RDKit molecule object.
        :returns: Updated RDKit molecule object."""
        name = "%s_p_%s" % (self.reactionId, self.num_products)
        mol.SetProp("_Name", name)

        return mol

    def _getUniqueProducts(self):
        """Retrieve unique products of the reaction, one at a time."""
        # Save SMILES of products in a set in order to keep only the unique
        # products of the reaction
        if not self.unique:
            self.unique = set()
            self.num_products = 0
        # end if

        while True:
            try:
                # Get reaction products
                # Python 3
                mols = next(self.products)
                # end if
            except StopIteration:
                break
            # end try-except
            # Generate SMILES
            smiles = ((Chem.MolToSmiles(m, isomericSmiles=True), idx)
                      for idx, m in enumerate(mols))
            # Keep only unique compounds
            for s in smiles:
                self.num_products += 1
                if not s[0] in self.unique:
                    mol = mols[s[1]]
                    # Keep compounds that are larger than 1 Atom
                    if mol.GetNumAtoms() > 1:
                        # 20140407 --ckannas
                        # Convert Chem.Mol to Chem.PropertyMol.PropertyMol
                        # to preserve properties when pickling and unpickling
                        mol = PropertyMol(mol)
                        mol = self._setProductName(mol)
                        mol = self._updateHistoryTree(mol)
                        # Add SMILES in unique set...
                        self.unique.add(s[0])
                        yield mol
                    # end if
                # end if
            # end for
            # Free memory unused
            del smiles
            del mols
            gc.collect()
        # end while
        logger.info("Total number of unique products: %d" % (len(self.unique)))
        logger.info("Total number of products: %d" % self.num_products)
        # Free memory unused
        gc.collect()

        return

    def getAllUniqueProducts(self):
        """Retrieve ALL unique products."""
        logger.info("This can be time consuming...")
        # Progress Bar
        widgets = ["Reaction Id: %4s - Retrieve All Unique Products: "
                   % self.reactionId,
                   progressbar.Percentage(), " ",
                   progressbar.ETA(), " ",
                   progressbar.Bar()]
        pbar = progressbar.ProgressBar(widgets=widgets,
                                       max_value=progressbar.UnknownLength)
        #
        start_time = datetime.now()
        products = self._getUniqueProducts()
        #
        # Get ALL results
        # Progress bar start
        pbar.start()
        for m in products:
            # Keep the pickled version of each molecule
            yield PickleUtils.pickleObjToStr(m)
            # # Update progress bar's maximum value
            # if self.num_products >= pbar.maxval:
            #     pbar.maxval += (pbar.maxval / 2)
            # # end if
            pbar.update(self.num_products)
        # end for
        # Progress bar finish
        pbar.finish()
        #
        elapsed_time = datetime.now() - start_time
        logger.info("Time elapsed: %s" % elapsed_time)

        return

    def __getstate__(self):
        """Return a picklable state of the class object"""
        # print("Pickling in process... %s" % (self.name))
        # convert iterator to list
        self.products = list(self.products)
        # end if

        return self.__dict__

    def __setstate__(self, d):
        """Set the state of the class object during unpickle.

        :param d: Pickled state dictionary."""
        # print("I'm being unpickled with these values: %s", d)
        # Set state...
        self.__dict__ = d
        # Convert tuple to iterator...
        self.products = iter(self.products[self.num_products:])

        return


class ReactionsContainer(object):
    """A Reactions container is an object that contains several instances of
    ReactionContainers, and provides functionality to enumerate
    a set of reactions."""

    def __init__(self):
        """Constructor method"""
        self.reactions = list()
        self.numReactions = 0

        return

    def displayInfo(self):
        """Reactions information for display purpose."""
        info = ""
        for r in self.reactions:
            info += "%s\n" % (r.displayInfo())
        # end for

        return info

    def addReaction(self, newReaction):
        """Add a new reaction.

        :param newReaction: ReactionContainer instance."""
        try:
            self.reactions.append(newReaction)
            self.numReactions += 1
        except ValueError:
            pass
        # end try-except

        return

    def saveRxnFiles(self, outDir):
        """Save Reactions to Rxn files. One file per reaction.

        :param outDir: Output directory path."""
        outDir = os.path.abspath(outDir)
        if not os.path.exists(outDir):
            os.mkdir(outDir)
        elif not os.path.isdir(outDir):
            raise Error("%s is not a directory." % outDir)
        for reaction in self.reactions:
            rxnFile = "reaction_%s.rxn" % reaction.reactionId
            rxnFile = os.path.join(outDir, rxnFile)
            reaction.saveRxnFile(rxnFile)
        logger.info("%d reaction files created in %s."
                    % (self.numReactions, outDir))

        return

    def _getMolsFGs(self, fgs, mols, result):
        """Find functional groups within available compounds.

        :param fgs: Functional Groups SMARTS patthern list.
        :param mols: List of RDKit molecules.
        :param result: Dictionary to store functional group matches."""
        # if not available functional group
        if not fgs:
            return
        # end if
        # for each functional group...
        for x in fgs:
            # functional group SMARTS pattern
            patt = x.pattern
            # for each molecule that has a substructure match save its
            # position in the molecules list
            tmp = [i for i, m in enumerate(mols) if m.HasSubstructMatch(patt)]
            # if there are functional groups then,
            if len(tmp):
                # 1. Save list of molecules list indices and functional group
                # SMARTS pattern to the resuts dictionary under the label
                # of the functional group
                result[x.label] = molsFG(mols=tmp, pattern=patt)
                # info
                logger.debug("%s %s %d"
                             % (x.label,
                                Chem.MolToSmarts(result[x.label].pattern,
                                                 isomericSmiles=True),
                                len(result[x.label].mols)))
                # 2. Repeat for its children...
                self._getMolsFGs(x.children, mols, result)
            # end if
        # end for

        return

    def _getRxnsFGs(self, fgs, rxns, result):
        """Find  Functional Groups within available reactants.

        :param fgs: Functional Groups SMARTS patthern list.
        :param rxns: List of reactions.
        :param result: Dictionary to store functional group matches."""
        # if not available functional group
        if not fgs:
            return
        # end if
        # for each functional group...
        for x in fgs:
            # functional group SMARTS pattern
            patt = x.pattern
            # for each reaction...
            for ReacCon_idx, ReacCon in enumerate(rxns):
                # Store information for each reactant's
                # functional group substracture matches
                react_FG = reactFG(reactionIdx=ReacCon_idx, reactants=set())
                # for each reactant template of the reaction...
                for i in range(ReacCon.rxn.GetNumReactantTemplates()):
                    # Get reactant template
                    m = ReacCon.rxn.GetReactantTemplate(i)
                    #
                    # Reactant template is a SMARTS query molecule
                    # and has to be converted to a SMILES molecule
                    # because substructure matching of SMARTS to SMARTS
                    # is checking if queries on the atoms are same
                    # while substructure matching of SMARTS to SMILES
                    # is checking if atoms satisfy the query
                    #
                    # 1. Get the SMILES of the reactant template
                    # 2. Create a new molecule using the SMILES
                    #
                    m = Chem.MolFromSmiles(
                        Chem.MolToSmiles(m, isomericSmiles=True))
                    if not m:
                        # Remove & from SMARTS to create a valid SMILES string
                        m = ReacCon.rxn.GetReactantTemplate(i)
                        smiles = Chem.MolToSmarts(m).replace("&-", "-")
                        smiles = smiles.replace("&+", "+")
                        smiles = smiles.replace("#1", "H")
                        m = Chem.MolFromSmiles(smiles)
                    # end if
                    if m:
                        m = AllChem.RemoveHs(m)
                    # end if
                    # Check reactant for substructures of the
                    # functional group pattern
                    if m and m.HasSubstructMatch(patt):
                        react_FG.reactants.add(i)
                    # end if
                # end for
                # If there are matching patterns in the reactants
                # of the reaction add it to the reactions
                # functional group data structure
                if len(react_FG.reactants):
                    if x.label not in result:
                        result[x.label] = rxnFG(reaction=[react_FG],
                                                pattern=patt)
                    else:
                        result[x.label].reaction.append(react_FG)
                    # end if
                # end if
            # end for
            if x.label in result:
                # info
                logger.debug("%s %s %d"
                             % (x.label,
                                Chem.MolToSmarts(result[x.label].pattern,
                                                 isomericSmiles=True),
                                len(result[x.label].reaction)))
                # 20140319 --ckannas
                # No need at the moment since query matching does not work...
                # Repeat for its children...
                # self._getRxnsFGs(x.children, rxns, result)
            # end if
        # end for

        return

    def findReactants(self, molsList):
        """Find possible reactants for each reaction using functional groups.

        :param molsList: List of RDKit molecules."""
        # Timing
        start_time = datetime.now()
        # Build functional groups hierarchy structure
        fileFGs = os.path.join(Config.DataDir, 'Functional_Group_Hierarchy.txt')
        fgs = FunctionalGroups.BuildFuncGroupHierarchy(fileNm=fileFGs)
        # Find functional groups of molecules
        logger.debug("Find functional groups of molecules")
        logger.debug("-----------------------------------")
        self.molFGS = {}
        self._getMolsFGs(fgs, molsList, self.molFGS)
        # Find functional groups of reactions
        logger.debug("Find functional groups of reactions")
        logger.debug("-----------------------------------")
        self.reactionFGS = {}
        self._getRxnsFGs(fgs, self.reactions, self.reactionFGS)

        # Dictionary to keep information about the possibility of
        # enumerating reactions
        self.possible_EnumReactions = dict()
        logger.debug("Functional Group No.Mols No.Reactions")
        logger.debug("-----------------------------")
        for fg_label in sorted(self.molFGS):
            if fg_label in self.reactionFGS:
                logger.debug("%s %d %d"
                             % (fg_label, len(self.molFGS[fg_label].mols),
                                len(self.reactionFGS[fg_label].reaction)))
                for r in self.reactionFGS[fg_label].reaction:
                    logger.debug("\tReaction Index: %s Reactant(s) ID: %s"
                                 % (r.reactionIdx, r.reactants))
                    if r.reactionIdx in self.possible_EnumReactions.keys():
                        self.possible_EnumReactions[r.reactionIdx] |= \
                            r.reactants
                    else:
                        self.possible_EnumReactions[r.reactionIdx] = \
                            r.reactants
                    # end if
                # end for
            # end if
        # end for
        # Find which reactions are not possible to be enumerated
        to_remove = list()
        for reactionIdx, matchedReactants in \
                self.possible_EnumReactions.items():
            reaction = self.reactions[reactionIdx]
            # Identify which reactions can be enumerated by using this
            # information alongside with substracture matching to the
            # available molecules if the number of matching reactants is less
            # than the actual number of reaction's reactants
            if reaction.numReactants >= len(matchedReactants):
                matchingCompounds = reaction.findMatchingReactants(molsList)
                if not reaction.IsEnumerate(matchingCompounds):
                    to_remove.append(reactionIdx)
                # end if
            # end if
        # end for
        logger.debug("Non enumeratable: %s" % to_remove)
        # Remove those reactions indices from possible
        # enumeratable reactions list
        [self.possible_EnumReactions.pop(rid) for rid in to_remove]
        # convert to list, keep only reaction indices
        self.possible_EnumReactions = self.possible_EnumReactions.keys()
        logger.info("Elapsed time: %s" % (datetime.now() - start_time))
        logger.info("No. of possible reactions that can be enumerated: "
                    "%d out of %d" % (len(self.possible_EnumReactions),
                                      len(self.reactions)))
        logger.debug("Possible reactions list indices: %s\n"
                     % self.possible_EnumReactions)

        return

###############################################################################
#                                                                             #
#                            Utility Methods                                  #
#                                                                             #
###############################################################################


def _parallelRunReactants(host, port, rxn, chains, productHistory):
    """Parallel RunReactants.

    :param port: Port number of the TCPServerLogger.
    :param rxn: RDKit reaction.
    :param chains: List of RDKit molecules to be used as reactants.
    :param productHistory: JSON string of the production history.
    :returns: A temporary pickled file with the results of the enumeration."""
    # Enable Network Logger
    rootLogger = logging.getLogger('')
    rootLogger.setLevel(logging.DEBUG)
    socketHandler = logging.handlers.SocketHandler(host, port)
    # don't bother with a formatter, since a socket handler sends the event as
    # an unformatted pickle
    rootLogger.addHandler(socketHandler)

    try:
        # RunReactants...
        prodSets = rxn.RunReactants(chains)
        # Add history information to all generated products
        fd, tname = tempfile.mkstemp()
        prods_num = 0
        unique_smiles = set()
        with os.fdopen(fd, "wb") as f:
            for prods in prodSets:
                for prod in prods:
                    # Remove hydrogens and sanitize
                    try:
                        prod = Chem.RemoveHs(prod)
                    except ValueError:
                        # logger.warning("Could not remove Hs: %s"
                        #                % (Chem.MolToSmiles(prod,
                        #                                isomericSmiles=True)))
                        pass
                    finally:
                        try:
                            Chem.SanitizeMol(prod)
                            # Get canonical isomeric SMILES
                            smiles = Chem.MolToSmiles(prod, isomericSmiles=True)
                            # Check if this SMILES was encountered before...
                            if smiles not in unique_smiles:
                                # If not then...
                                # Convert to PropertyMol for easy transfer...
                                prod = PropertyMol(prod)
                                prod.SetProp("History", productHistory)
                                # Pickle it to the temporary file
                                PickleUtils.pickleObjToFile(prod, f)
                                prods_num += 1
                                # Add its SMILES to the set of unique SMILES
                                unique_smiles.add(smiles)
                            # end if
                        except:
                            # Skip molecule that can't be sanitized...
                            # logger.warning("Could not sanitize: %s"
                            #                % (Chem.MolToSmiles(prod,
                            #                            isomericSmiles=True)))
                            pass
                        # end try-except
                    # end try-except
                # end for
            # end for
        # end with
        # clean up
        del prodSets
        del unique_smiles
    finally:
        socketHandler.close()
    # end try finally

    return tname, prods_num
