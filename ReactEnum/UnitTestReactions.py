from __future__ import print_function

"""
Unit testing of Reactions module.

.. moduleauthor:: Christos Kannas <chriskannas@gmail.com>

Copyright 2017, Christos Kannas.
"""

__version__ = "0.1.0"

# Python Imports
import os
import unittest

# MOReactMolDesign Imports
from MOReactMolDesign import Config
from MOReactMolDesign.CLI_Tools.Support import FileParser


class TestCase(unittest.TestCase):
    """Test case for Reactions.py"""

    def setUp(self):
        """Setup environment."""
        # Parse SMILES file
        self.smilesFile = os.path.join(Config.DataDir,
                                       "N8BioHubFeedStocksUsefulReactants.smi")
        self.molsList = FileParser.SMILESFileReader(self.smilesFile)
        # Reactions
        self.reactionsFile = os.path.join(Config.DataDir,
                                          "N8BioHubReactions.smirks")
        self.reactionsList = FileParser.SMIRKSFileParser(self.reactionsFile)

        return

    def testReactionFileParsing(self):
        """Test parsing reaction SMIRKS file."""
        self.assertEqual(self.reactionsList.numReactions,
                         24,
                         "Failed parsing '%s' correctly" % self.reactionsFile)

        return

    def testReactionReactantMatching(self):
        """Test reaction's reactant matching."""
        # Use Ethoxylation reaction
        reaction = self.reactionsList.reactions[1]
        reactants = reaction.findMatchingReactants(self.molsList)
        self.assertEqual(reaction.IsEnumerate(reactants),
                         1,
                         "Failed to find reaction enumerable")

        return

    def testEnumerateReaction(self):
        """Test enumerate reaction and get all unique products."""
        # Use Ethoxylation reaction
        reaction = self.reactionsList.reactions[1]
        reactants = reaction.findMatchingReactants(self.molsList)
        reaction.EnumerateReaction(reactants)
        unique_products = reaction.getAllUniqueProducts()
        # Convert unique_products to a list and check if it has 177 molecules
        self.assertEqual(len(list(unique_products)),
                         177,
                         "Failed to create the same amount of unique products")

        return


if __name__ == '__main__':
    unittest.main()
